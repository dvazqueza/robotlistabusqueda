//'use strict';

/* Controllers */

 // Variables para los POST asíncronos.
var postLanzarRobot = '';
var postPollingProceso = '';
var postReporte = '';

/**
 * controlador del lanzador del robot.
 */
function LanzarobotCtrl($scope, $http, $location, sharedCreditos, $cookies, $cookieStore) {
    postLanzarRobot = 'http://' + $location.host() + ':' + $location.port() + '/robotlb/robotlb/lanzarrobot';

    $scope.lanzarobot = function() {
        //$("#divButton").addClass("bigBlackWaiting");
        $("#lanzarobot").attr("disabled", "disabled");

        var parametros = $.param({usuario: $scope.usuario, passwd: $scope.passwd, ntoken: $scope.token});
        console.log("Lanzando robot...");

        $http.post(
          postLanzarRobot, 
          parametros, 
          {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
          }
        ).
        success(function(response, estatus) {
        	console.log('Success!!! , response=' + JSON.stringify(response) + " | estatus=" + estatus);
            var resultado = response.robot;
            var caderror = response.caderror;
            var cred = response.creditos;
            var tiempo = response.tiempo;

            if (resultado == "falla") {
            	if (caderror == "errorobtenercreditos") {
                    alert("Error al obtener los créditos de la base de datos.");
                }
            	else {
                    alert("Error: " + caderror);
            	}
            }
            else if (resultado == "exito") {
                if (caderror != null && caderror.length > 0) {
                	if (caderror == "noexistencreditos") {
                        sharedCreditos.setCreditos(cred);
                        alert("No existen créditos disponibles.");
                    }
                	else {
                		alert("Error: " + caderror);
                	}
                }
                else if (cred != null && cred > 0) {
                	console.log('Existen creditos y salio de operacion...');
//                    console.log('Lanzando polling...');
//                    sharedCreditos.setCreditos(cred);
//                    sharedCreditos.setTiempo(tiempo);
//                    $location.path("/pollingproceso");
                }
            }

        })
        .error(function(response, estatus) {
            console.log('Error!!! , data=' + JSON.stringify(response) + " | estatus=" + estatus);
            alert("Ocurrió un error consulte a su administrador!");
        });
    };

}
LanzarobotCtrl.$inject = ['$scope', '$http', '$location', 'sharedCreditos', '$cookies', '$cookieStore'];


/**
 * controlador del proceso de polling hacia el proceso de pago de creditos.
 */
function PollingprocesoCtrl($scope, $http, $location, sharedCreditos, $timeout) {
    $scope.creditos = sharedCreditos.getCreditos();
    $scope.procesados = 0;
    $scope.bitacoras;
    $("#tablaResutado").hide();
    $("#leyendanoerrores").hide();

    postPollingProceso = 'http://' + $location.host() + ':' + $location.port() + '/robotlb/robotlb/pollingproceso';
    postReporte = 'http://' + $location.host() + ':' + $location.port() + '/robotlb/robotlb/reporte';

    var self = this;
    this.keepOnGoing = false;

    this.startPolling = function() {
      this.keepOnGoing = true;
      this.poll();
    };

    this.stopPolling = function() {
      this.keepOnGoing = false;
    };

    this.poll = function() {
        console.log("Polling proceso...");
    	var parametros = $.param({tiempo: sharedCreditos.getTiempo()});

        sharedCreditos.setIteracion(sharedCreditos.getIteracion() + 1);
    	console.log("Iteracion:" + sharedCreditos.getIteracion());
        console.log("Totales:" + sharedCreditos.getCreditos());
        if (Number($scope.procesados) >= Number(sharedCreditos.getCreditos())) {
            self.stopPolling();
            self.armaTablaResultado();
        }
        else if (Number($scope.procesados) < Number(sharedCreditos.getCreditos()) && 
                 Number(sharedCreditos.getIteracion()) >= Number(sharedCreditos.getMaxIteraciones())) {
            self.stopPolling();
            alert("Error no esperado al monitorear el proceso de pago.");
        }
        else {
          $http.post(
              postPollingProceso, 
              parametros, 
              {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
              }
            ).
            success(function(response, estatus) {
                console.log('Success!!! , response=' + JSON.stringify(response) + " | estatus=" + estatus);
                var resultado = response.polling;
                var procs = response.procesados;
                var caderror = response.caderror;
            	console.log("Procesados:" + procs);

                if (resultado == "falla") {
                    self.stopPolling();
                    alert("Error: " + caderror);
                }
                else if (resultado == "exito") {
                    if (caderror != null && caderror.length > 0) {
                        self.stopPolling();
                        alert("Error: " + caderror);
                    }
                    else {
                    	$scope.procesados = procs;
                    }
                }
//                if (self.keepOnGoing) {
//                    $timeout(self.poll, 2000);
//                }
            })
            .error(function(response, estatus) {
                console.log('Error!!! , data=' + JSON.stringify(response) + " | estatus=" + estatus);
                sharedCreditos.setIteracion(sharedCreditos.getIteracion() + 1);
                self.stopPolling();
                alert("Ocurrió un error consulte a su administrador!");
          });

          if (self.keepOnGoing) {
              $timeout(self.poll, 4000);
          }
        }
    };


    this.armaTablaResultado = function() {
        console.log("Armando tabla resultado ...");
    	var parametros = $.param({tiempo: sharedCreditos.getTiempo()});

    	if (!sharedCreditos.isReporteArmado()) {
            console.log("pidiendo reporte ...");
            sharedCreditos.setReporteArmado(true);
            $http.post(
                  postReporte, 
                  parametros, 
                  {
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                  }
                ).
                success(function(response, estatus) {
                    console.log('Success!!! , estatus=' + estatus);
                    if (response.noerrores != null && response.noerrores != undefined) {
                        $("#leyendanoerrores").show();
                    }
                    else {
                        $("#tablaResutado").show();
                        $scope.bitacoras = response;
                    }
                })
                .error(function(response, estatus) {
                    console.log('Error!!! , data=' + JSON.stringify(response) + " | estatus=" + estatus);
                    alert("Ocurrió un error al crear el reporte, consulte a su administrador!");
                });
        }
    };

    if ($scope.creditos > 0) {
        this.startPolling();
    }

}
PollingprocesoCtrl.$inject = ['$scope', '$http', '$location', 'sharedCreditos', '$timeout'];
