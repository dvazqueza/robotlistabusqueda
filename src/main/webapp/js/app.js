'use strict';

// Declare app level module which depends on filters, and services
angular.module('robotlb', ['ngCookies'])
  .service('sharedCreditos', function () {
      var creditos = -1;
      var tiempo = "";
      var iteracion = 0;
      var maxIteraciones = 500;
      var reporteArmado = false;
      var polling = false;

      return {
          getCreditos: function() {
              return creditos;
          },
          setCreditos: function(value) {
        	  creditos = value;
          },
          getTiempo: function() {
              return tiempo;
          },
          setTiempo: function(value) {
        	  tiempo = value;
          },
          getIteracion: function() {
              return iteracion;
          },
          setIteracion: function(value) {
        	  iteracion = value;
          },
          getMaxIteraciones: function() {
              return maxIteraciones;
          },
          setMaxIteraciones: function(value) {
        	  maxIteraciones = value;
          },
          isReporteArmado: function() {
              return reporteArmado;
          },
          setReporteArmado: function(value) {
        	  reporteArmado = value;
          }
      };
  })
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/lanzarrobot', {templateUrl: 'partials/lanzarrobot.html', controller: LanzarobotCtrl});
    $routeProvider.when('/pollingproceso', {templateUrl: 'partials/pollingproceso.html', controller: PollingprocesoCtrl});
    $routeProvider.otherwise({redirectTo: '/lanzarrobot'});
  }]);
