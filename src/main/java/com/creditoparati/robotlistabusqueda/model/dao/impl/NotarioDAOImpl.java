package com.creditoparati.robotlistabusqueda.model.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.creditoparati.robotlistabusqueda.model.Notario;
import com.creditoparati.robotlistabusqueda.model.dao.NotarioDAO;

/**
 * Implementaci&oacute;n del dao de Notario.
 * @author Edgar Deloera
 * @author Daniel Vazquez
 */
public class NotarioDAOImpl extends HibernateDaoSupport implements NotarioDAO {

    @Override
    public List<Notario> getByNumNotaria(final long numNotaria) {
        return (List<Notario>) getHibernateTemplate().executeFind(new HibernateCallback() {
            public List<Notario> doInHibernate(Session session) throws HibernateException {
                Criteria crit = session.createCriteria(Notario.class);
                crit.add(Restrictions.eq("numNotaria", numNotaria));
                return (List<Notario>) crit.list();
            }
        });
    }

    @Override
    public Notario getByNombreNotario(final String nombreNotario) {
    	final String[] elemenNombreNotario = nombreNotario.split(" ");

    	return (Notario) getHibernateTemplate().execute(new HibernateCallback<Notario>() {
            public Notario doInHibernate(Session session) throws HibernateException {
                Criteria crit = session.createCriteria(Notario.class);
                for(String elemento:elemenNombreNotario) {
                	logger.debug("agregando elemento = ["+ elemento +"]");
                    crit.add(
                    	Restrictions.or(
                    			(Restrictions.or(
                    				Restrictions.like("nombre", "%" + elemento + "%"),
                    				Restrictions.like("apellidoPaterno", "%" + elemento + "%"))), 
                            Restrictions.like("apellidoMaterno", "%" + elemento + "%")));
                }
                return (Notario) crit.uniqueResult();
            }
        });
    }
}
