package com.creditoparati.robotlistabusqueda.model.dao;

import java.util.List;

import com.creditoparati.robotlistabusqueda.model.Bitacora;

/**
 * Interfaz del DAO de Bitacora.
 * @author Edgar Deloera
 */
public interface BitacoraDAO {

    /**
     * Obtiene una entidad Bitacora por id.
     * @param idExpediente El id del Expedien te a obtener
     * @return La entidad Expediente.
     */
    Bitacora getById(long idBitacora) throws Exception;
    
    /**
     * Salva un objeto bitacora en la base de datos.
     * @param bitacora
     */
    void save(Bitacora bitacora) throws Exception;

    /**
     * Elimina el objeto relacionado con el id proporcionado de la tabla
     * @throws Exception
     */
    void delete(long id) throws Exception;
    
    /**
     * Obtiene el numero total de pagos procesados despues de cierto tiempo. 
     * @param tiempo El tiempo para realizar la seleccion del total de pagos.
     * @return El total de pagos procesados hasta ese tiempo.
     * @throws Exception
     */
    int getTotalExpedientesProcesadosPorTiempo(String tiempo) throws Exception;

    /**
     * Encuentra los objetos Bitacora que esten en estado de fallo en un cierto tiempo.
     * @param tiempo La cadena que dicta el tiempo.
     * @return El listado de objetos encontrados.
     * @throws Exception
     */
    List<Bitacora> findBitacorasErroneasPorTiempo(String tiempo) throws Exception;

    /**
     * Obtiene el siguiente numero de Bitacora en la tabla
     * @throws Exception
     */
    public Long getSiguienteIdBitacora() throws Exception;
}
