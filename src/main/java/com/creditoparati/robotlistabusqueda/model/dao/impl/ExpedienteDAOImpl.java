package com.creditoparati.robotlistabusqueda.model.dao.impl;

import java.util.List;
import java.math.BigInteger;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.creditoparati.robotlistabusqueda.middleware.services.error.FetchExpedientesErrorException;
import com.creditoparati.robotlistabusqueda.model.Expediente;
import com.creditoparati.robotlistabusqueda.model.dao.ExpedienteDAO;

/**
 * Implementaci&oacute;n del dao de Expedientes.
 * @author Edgar Deloera
 * @author Daniel Vazquez
 */
public class ExpedienteDAOImpl extends HibernateDaoSupport implements ExpedienteDAO {

    @Override
    public List<Expediente> loadAll() throws FetchExpedientesErrorException {
        try {
        	List<Expediente> expedientes = this.getHibernateTemplate().loadAll(Expediente.class);
        	return expedientes;
        }
    	catch(Exception e) {
    		throw new FetchExpedientesErrorException(e);
    	}
    }

    @Override
    public int getTotalExpedientes() throws FetchExpedientesErrorException {
        try {
	    	Integer registros = (Integer) getHibernateTemplate().execute(new HibernateCallback<Integer>() {
	            public Integer doInHibernate(Session session) throws HibernateException {
	                SQLQuery query = session.createSQLQuery("SELECT count(*) FROM v_sm_expediente_avanzar_etapa");
	                return ((BigInteger) query.uniqueResult()).intValue();
	            }
	        });
	        return registros.intValue();
        }
    	catch(Exception e) {
    		throw new FetchExpedientesErrorException(e);
    	}

    }

    @Override
    public void delete(String id) throws FetchExpedientesErrorException
    {
    	try {
    		getHibernateTemplate().delete(getHibernateTemplate().load(Expediente.class,id));
        }
    	catch(Exception e) {
    		throw new FetchExpedientesErrorException(e);
    	}
    }
    
    @Override
    public void save(Expediente transientInstance) throws FetchExpedientesErrorException {
    	try {
            this.getHibernateTemplate().flush();
            Long sigIdExpediente = this.getSiguienteIdExpediente();
            transientInstance.setNumeroSolicitud(sigIdExpediente.toString());
            this.getHibernateTemplate().save(transientInstance);
	    }
		catch(Exception e) {
			throw new FetchExpedientesErrorException(e);
		}

    }
    
    
    @Override
    public Expediente getById(final String idExpediente) throws FetchExpedientesErrorException {
    	try {
	    	return (Expediente) getHibernateTemplate().execute(new HibernateCallback<Expediente>() {
	            public Expediente doInHibernate(Session session) throws HibernateException {
	                Criteria crit = session.createCriteria(Expediente.class);
	                    crit.add(Restrictions.eq("numeroSolicitud", idExpediente));
	                return (Expediente) crit.uniqueResult();
	            }
	        });
	    }
		catch(Exception e) {
			throw new FetchExpedientesErrorException(e);
		}
    }
    
    /**
     * Obtiene el siguiente id a usar en la tabla de expediente.
     * @return El id siguiente.
     */
    public Long getSiguienteIdExpediente() throws FetchExpedientesErrorException{
        try {
	    	Integer registros = getHibernateTemplate().execute(new HibernateCallback<Integer>() {
	            public Integer doInHibernate(Session session) throws HibernateException {
	                SQLQuery query = session.createSQLQuery("SELECT count(*) FROM v_sm_expediente_avanzar_etapa");
	                return ((BigInteger) query.uniqueResult()).intValue();
	            }
	        });
	        long sigIdExpediente = registros.intValue() + 1;
	        return new Long(sigIdExpediente);
	    }
		catch(Exception e) {
			throw new FetchExpedientesErrorException(e);
		}
    }
}
