package com.creditoparati.robotlistabusqueda.model.dao.impl;

import java.math.BigInteger;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.creditoparati.robotlistabusqueda.model.Desarrolladora;
import com.creditoparati.robotlistabusqueda.model.dao.DesarrolladoraDAO;

/**
 * Implementaci&oacute;n del dao de Desarrolladora.
 * @author Edgar Deloera
 * @author Daniel Vazquez
 */
public class DesarrolladoraDAOImpl extends HibernateDaoSupport implements DesarrolladoraDAO {

    @Override
    public Desarrolladora getByNombre(final String nombre) {
        return (Desarrolladora) getHibernateTemplate().execute(new HibernateCallback<Desarrolladora>() {
            public Desarrolladora doInHibernate(Session session) throws HibernateException {
                Criteria crit = session.createCriteria(Desarrolladora.class);
                crit.add(Restrictions.like("nombre", "%" + nombre + "%"));
                return (Desarrolladora) crit.uniqueResult();
            }
        });
    }

    @Override
    public Desarrolladora getByNombreCorto(final String nombreCorto) {
        return (Desarrolladora) getHibernateTemplate().execute(new HibernateCallback<Desarrolladora>() {
            public Desarrolladora doInHibernate(Session session) throws HibernateException {
                Criteria crit = session.createCriteria(Desarrolladora.class);
                crit.add(Restrictions.like("nombreCorto", "%" + nombreCorto + "%"));
                return (Desarrolladora) crit.uniqueResult();
            }
        });
    }
    
    @Override
    public int getTotalDesarrolladoras() throws Exception {
        Integer registros = (Integer) getHibernateTemplate().execute(new HibernateCallback<Integer>() {
            public Integer doInHibernate(Session session) throws HibernateException {
                SQLQuery query = session.createSQLQuery("SELECT count(*) FROM v_sm_desarrolladora");
                return ((BigInteger) query.uniqueResult()).intValue();
            }
        });
        return registros.intValue();
    }
    
    @Override
    public List<Desarrolladora> loadAll() throws Exception {
        List<Desarrolladora> desarrolladoras = this.getHibernateTemplate().loadAll(Desarrolladora.class);
        return desarrolladoras;
    }
}
