package com.creditoparati.robotlistabusqueda.model.dao;

import java.util.List;

import com.creditoparati.robotlistabusqueda.middleware.services.error.FetchExpedientesErrorException;
import com.creditoparati.robotlistabusqueda.model.Expediente;

/**
 * Interfaz del DAO de Expediente.
 * @author Edgar Deloera
 * @author Daniel Vazquez
 */
public interface ExpedienteDAO {

    /**
     * Obtiene una entidad Expediente por id.
     * @param idExpediente El id del Expedien te a obtener
     * @return La entidad Expediente.
     */
    Expediente getById(String idExpediente) throws FetchExpedientesErrorException;
    
    /**
     * Obtiene un listado con todos los creditos que se encuentran en la vista.
     * @return El listado de todos los creditos.
     */
    List<Expediente> loadAll() throws FetchExpedientesErrorException;

    /**
     * Obtiene el numero total de creditos disponibles en la BD para su procesamiento.
     * @return El numero total de creditos disponibles.
     * @throws Exception
     */
    int getTotalExpedientes() throws FetchExpedientesErrorException;
    
    /**
     * Elimina el objeto relacionado con el id proporcionado de la tabla
     * @throws Exception
     */
    void delete(String id) throws FetchExpedientesErrorException;
    
    /**
     * Inserta un nuevo Expediente en la tabla
     * @throws Exception
     */
    void save(Expediente transientInstance) throws FetchExpedientesErrorException;
   
    /**
     * Obtiene el siguiente numero de Expediente en la tabla
     * @throws Exception
     */
    public Long getSiguienteIdExpediente() throws FetchExpedientesErrorException;
}
