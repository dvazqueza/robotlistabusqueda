package com.creditoparati.robotlistabusqueda.model.dao;

import java.util.List;
import com.creditoparati.robotlistabusqueda.model.Desarrolladora;

/**
 * Interfaz del DAO de Desarrolladora.
 * @author Edgar Deloera
 * @author Daniel Vazquez
 */
public interface DesarrolladoraDAO {

    /**
     * Obtiene una entidad Desarrolladora por su nombre.
     * @param nombre El nombre para obtener la entidad Desarrolladora.
     * @return La entidad Desarrolladora.
     */
    Desarrolladora getByNombre(String nombre) throws Exception;

    /**
     * Obtiene una entidad Desarrolladora por su nombreCorto.
     * @param nombre El nombre para obtener la entidad Desarrolladora.
     * @return La entidad Desarrolladora.
     */
    Desarrolladora getByNombreCorto(String nombreCorto) throws Exception;
    
    /**
     * Obtiene las entidades de Desarrolladora que existen en la tabla .
     * @return La entidad Desarrolladora.
     */
    List<Desarrolladora> loadAll() throws Exception;
    
    /**
     * Obtiene el numero de Desarrolladora que existen en la tabla .
     * @return La entidad Desarrolladora.
     */
    int getTotalDesarrolladoras() throws Exception;

}
