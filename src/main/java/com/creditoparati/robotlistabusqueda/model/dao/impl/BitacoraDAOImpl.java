package com.creditoparati.robotlistabusqueda.model.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.creditoparati.robotlistabusqueda.model.Bitacora;
import com.creditoparati.robotlistabusqueda.model.dao.BitacoraDAO;

/**
 * Implementaci&oacute;n del dao de Bitacora.
 * @author Edgar Deloera
 */
public class BitacoraDAOImpl extends HibernateDaoSupport implements BitacoraDAO {

    /**
     * Log de la clase
     */
    private static final Logger log = Logger.getLogger(BitacoraDAOImpl.class);

    @Override
    public void save(Bitacora bitacora) throws Exception {
        log.debug("######### Insertando bitacora ...");

        this.getHibernateTemplate().flush();
        Long sigIdBitacora = this.getSiguienteIdBitacora();
        bitacora.setId(sigIdBitacora);
        this.getHibernateTemplate().save(bitacora);
    }
    
    @Override
    public void delete(long id) throws Exception
    {
    	getHibernateTemplate().delete(getHibernateTemplate().load(Bitacora.class,id));
    }

    @Override
    public Bitacora getById(final long idBitacora) throws Exception {

    	return (Bitacora) getHibernateTemplate().execute(new HibernateCallback<Bitacora>() {
            public Bitacora doInHibernate(Session session) throws HibernateException {
                Criteria crit = session.createCriteria(Bitacora .class);
                    crit.add(Restrictions.eq("id", idBitacora));
                return (Bitacora) crit.uniqueResult();
            }
        });
    }
    
    /**
     * Obtiene el siguiente id a usar en la tabla de bitacora.
     * @return El id siguiente.
     */
    public Long getSiguienteIdBitacora() throws Exception {
        Integer registros = getHibernateTemplate().execute(new HibernateCallback<Integer>() {
            public Integer doInHibernate(Session session) throws HibernateException {
                SQLQuery query = session.createSQLQuery("SELECT count(*) FROM v_sm_bitacoras");
                return ((BigInteger) query.uniqueResult()).intValue();
            }
        });
        log.debug("######### #Regs actual en bitacora=" + registros.intValue());
        long sigIdBitacora = registros.intValue() + 1;
        log.debug("######### #Sig Id en bitacora=" + sigIdBitacora);
        return new Long(sigIdBitacora);
    }

    @Override
    public int getTotalExpedientesProcesadosPorTiempo(final String tiempo) throws Exception {
        Integer registros = getHibernateTemplate().execute(new HibernateCallback<Integer>() {
            public Integer doInHibernate(Session session) throws HibernateException {
                String q = "SELECT count(distinct(expediente)) FROM v_sm_bitacoras " +
                           // TODO "WHERE fecha_firma > convert(datetime, '" + tiempo + "', 109) ";
                           "WHERE fecha_firma > STR_TO_DATE('"+ tiempo +"' ,'%b %e %Y %H:%i:%s:%f') ";
                SQLQuery query = session.createSQLQuery(q);
                return ((BigInteger) query.uniqueResult()).intValue();
            }
        });
        return registros.intValue();
    }

    @Override
    public List<Bitacora> findBitacorasErroneasPorTiempo(final String tiempo) throws Exception {
        List<Bitacora> registros = getHibernateTemplate().execute(new HibernateCallback<List<Bitacora>>() {
            public List<Bitacora> doInHibernate(Session session) throws HibernateException {
                String q = "FROM Bitacora as b " +
                           // TODO "WHERE b.fechaFirma > convert(datetime, ?, 109) " +
                           "WHERE b.fechaFirma > STR_TO_DATE(?, '%b %e %Y %H:%i:%s:%f') " +
                           "AND b.estado = 0 ORDER BY b.numeroSolicitud, b.etapa ASC ";
                Query query = session.createQuery(q).setString(0, tiempo);
                List li = query.list();

                List<Bitacora> bitas = new ArrayList<Bitacora>();
                Bitacora bit = null;
                if (li.size() > 0) {
                    for (int i = 0; i < li.size(); i++) {
                        bit = (Bitacora) li.get(i);
                        bitas.add(bit);
                    }
                }
                return bitas;
            }
        });

        return registros;
    }

}
