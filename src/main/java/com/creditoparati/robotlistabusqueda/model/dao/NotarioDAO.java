package com.creditoparati.robotlistabusqueda.model.dao;

import java.util.List;

import com.creditoparati.robotlistabusqueda.model.Notario;

/**
 * Interfaz del DAO de Notario.
 * @author Edgar Deloera
 * @author Daniel Vazquez
 */
public interface NotarioDAO {

    /**
     * Obtiene una entidad Notario por su numero de notaria.
     * @param numNotaria El numero de notaria para obtener la entidad Notario.
     * @return La entidad Notario.
     */
    List<Notario> getByNumNotaria(long numNotaria);
    
    /**
     * Obtiene una entidad Notario por su nombre.
     * @param nombreNotario El nombre de notarioa para obtener la entidad Notario.
     * @return La entidad Notario.
     */
    Notario getByNombreNotario(String nombreNotario);

}
