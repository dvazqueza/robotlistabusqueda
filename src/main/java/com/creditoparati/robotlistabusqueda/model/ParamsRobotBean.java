package com.creditoparati.robotlistabusqueda.model;

/**
 * Representa parametros recibidos al robot.
 * @author Edgar Deloera
 */
public class ParamsRobotBean {

    private String usuario;
    private String passwd;
    private String ntoken;

    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getPasswd() {
        return passwd;
    }
    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }
    public String getNtoken() {
        return ntoken;
    }
    public void setNtoken(String ntoken) {
        this.ntoken = ntoken;
    }

}
