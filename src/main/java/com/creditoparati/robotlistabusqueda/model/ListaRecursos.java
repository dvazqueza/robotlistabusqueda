package com.creditoparati.robotlistabusqueda.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity @Table(name="v_fovissste_lista_recurso")
public class ListaRecursos {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "nsolicitud", nullable = false)
	private String numeroSolicitud;

    @Column(name = "interfase", nullable = false)
    private String interfase;

    @Column(name = "nombre_beneficiario", nullable = false)
    private String nombreBeneficiario;

    @Column(name = "rfc", nullable = true)
    private String rfc;

    @Column(name = "banco", nullable = true)
    private String banco;

    @Column(name = "clabe", nullable = true)
    private String clabe;

    @Column(name = "ncuenta", nullable = true)
    private String numeroCuenta;

    @Column(name = "nom_archivo_edo_cta", nullable = true)
    private String nomArchivoEdoCta;

    @Column(name = "domicilio", nullable = true)
    private String domicilio;

    @Column(name = "email_vendedor", nullable = true)
    private String emailVendedor;

    @Column(name = "email_comprador", nullable = true)
    private String emailComprador;

    @Column(name = "telefono_vendedor", nullable = false)
    private String telefonoVendedor;

    @Column(name = "pago_persona_distinta", nullable = false)
    private Boolean pagoPersonaDistinta;

    @Column(name = "nom_archivo_carta_cesion", nullable = true)
    private String nomArchivoCartaCesion;

    @Transient
    boolean update = false;

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getInterfase() {
        return interfase;
    }

    public void setInterfase(String interfase) {
        this.interfase = interfase;
    }

    public String getNombreBeneficiario() {
        return nombreBeneficiario;
    }

    public void setNombreBeneficiario(String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getClabe() {
        return clabe;
    }

    public void setClabe(String clabe) {
        this.clabe = clabe;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getNomArchivoEdoCta() {
        return nomArchivoEdoCta;
    }

    public void setNomArchivoEdoCta(String nomArchivoEdoCta) {
        this.nomArchivoEdoCta = nomArchivoEdoCta;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getEmailVendedor() {
        return emailVendedor;
    }

    public void setEmailVendedor(String emailVendedor) {
        this.emailVendedor = emailVendedor;
    }

    public String getEmailComprador() {
        return emailComprador;
    }

    public void setEmailComprador(String emailComprador) {
        this.emailComprador = emailComprador;
    }

    public String getTelefonoVendedor() {
        return telefonoVendedor;
    }

    public void setTelefonoVendedor(String telefonoVendedor) {
        this.telefonoVendedor = telefonoVendedor;
    }

    public Boolean getPagoPersonaDistinta() {
        return pagoPersonaDistinta;
    }

    public void setPagoPersonaDistinta(Boolean pagoPersonaDistinta) {
        this.pagoPersonaDistinta = pagoPersonaDistinta;
    }

    public String getNomArchivoCartaCesion() {
        return nomArchivoCartaCesion;
    }

    public void setNomArchivoCartaCesion(String nomArchivoCartaCesion) {
        this.nomArchivoCartaCesion = nomArchivoCartaCesion;
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

}
