package com.creditoparati.robotlistabusqueda.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity @Table(name="v_sm_expediente_avanzar_etapa")
public class Expediente {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "nsolicitud", nullable = false)
	private String numeroSolicitud;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "numcte", nullable = false)
    private String numeroCliente;

    @Transient
    boolean update = false;
    
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the numeroCliente
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}

	/**
	 * @param numeroCliente the numeroCliente to set
	 */
	public void setNumeroCliente(String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }
    
}
