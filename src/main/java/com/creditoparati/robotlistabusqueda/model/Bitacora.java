package com.creditoparati.robotlistabusqueda.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity @Table(name="v_sm_bitacoras")
public class Bitacora {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_bitacora", nullable = false)
//    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    @Column(name = "expediente", nullable = false)
	private String expediente;

    @Column(name = "firmado", nullable = true)
	private Boolean firmado;

    @Column(name = "fecha_firma", nullable = false)
    private Date fechaFirma;

    @Column(name = "notificado", nullable = true)
	private Boolean notificado;

    @Column(name = "estatus_bitacora", nullable = true)
	private Boolean estatusBitacora;

    @Column(name = "id_desarrolladora", nullable = true)
	private long idDesarrolladora;

    @Column(name = "id_notario", nullable = true)
	private long idNotario;
    
    @Column(name = "solicitud_no", nullable = true)
	private String numeroSolicitud;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }
    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }
	/**
	 * @return the expediente
	 */
	public String getExpediente() {
		return expediente;
	}
	/**
	 * @param expediente the expediente to set
	 */
	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}
	/**
	 * @return the firmado
	 */
	public Boolean getFirmado() {
		return firmado;
	}
	/**
	 * @param firmado the firmado to set
	 */
	public void setFirmado(Boolean firmado) {
		this.firmado = firmado;
	}
	/**
	 * @return the fechaFirma
	 */
	public Date getFechaFirma() {
		return fechaFirma;
	}
	/**
	 * @param fechaFirma the fechaFirma to set
	 */
	public void setFechaFirma(Date fechaFirma) {
		this.fechaFirma = fechaFirma;
	}
	/**
	 * @return the notificado
	 */
	public Boolean getNotificado() {
		return notificado;
	}
	/**
	 * @param notificado the notificado to set
	 */
	public void setNotificado(Boolean notificado) {
		this.notificado = notificado;
	}
	/**
	 * @return the estatusBitacora
	 */
	public Boolean getEstatusBitacora() {
		return estatusBitacora;
	}
	/**
	 * @param estatusBitacora the estatusBitacora to set
	 */
	public void setEstatusBitacora(Boolean estatusBitacora) {
		this.estatusBitacora = estatusBitacora;
	}
	/**
	 * @return the idDesarrolladora
	 */
	public long getIdDesarrolladora() {
		return idDesarrolladora;
	}
	/**
	 * @param idDesarrolladora the idDesarrolladora to set
	 */
	public void setIdDesarrolladora(long idDesarrolladora) {
		this.idDesarrolladora = idDesarrolladora;
	}
	/**
	 * @return the idNotario
	 */
	public long getIdNotario() {
		return idNotario;
	}
	/**
	 * @param idNotario the idNotario to set
	 */
	public void setIdNotario(long idNotario) {
		this.idNotario = idNotario;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


}
