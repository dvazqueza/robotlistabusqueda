package com.creditoparati.robotlistabusqueda.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity @Table(name="v_sm_notario")
public class Notario {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_notario", nullable = false)
    private long id;

    @Column(name = "ap_paterno_notario", nullable = false)
	private String apellidoPaterno;

    @Column(name = "ap_materno_notario", nullable = false)
    private String apellidoMaterno;

    @Column(name = "nom_notario", nullable = false)
	private String nombre;

    @Column(name = "dir_notario", nullable = false)
    private String direccion;

    @Column(name = "id_colonia", nullable = false)
    private long idColonia;

    @Column(name = "estado", nullable = false)
	private String estado;

    @Column(name = "cp", nullable = false)
	private String cp;
    
    @Column(name = "pais", nullable = false)
    private String pais;
    
    @Column(name = "num_notaria", nullable = false)
    private long numNotaria;

    @Column(name = "tel_1", nullable = false)
    private String telefono1;

    @Column(name = "tel_2", nullable = false)
    private String telefono2;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

	/**
	 * @return the apellidoPaterno
	 */
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	/**
	 * @param apellidoPaterno the apellidoPaterno to set
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	/**
	 * @return the apellidoMaterno
	 */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	/**
	 * @param apellidoMaterno the apellidoMaterno to set
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * @return the idColonia
	 */
	public long getIdColonia() {
		return idColonia;
	}

	/**
	 * @param idColonia the idColonia to set
	 */
	public void setIdColonia(long idColonia) {
		this.idColonia = idColonia;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the cp
	 */
	public String getCp() {
		return cp;
	}

	/**
	 * @param cp the cp to set
	 */
	public void setCp(String cp) {
		this.cp = cp;
	}

	/**
	 * @return the pais
	 */
	public String getPais() {
		return pais;
	}

	/**
	 * @param pais the pais to set
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}

	/**
	 * @return the numNotaria
	 */
	public long getNumNotaria() {
		return numNotaria;
	}

	/**
	 * @param numNotaria the numNotaria to set
	 */
	public void setNumNotaria(long numNotaria) {
		this.numNotaria = numNotaria;
	}

	/**
	 * @return the telefono1
	 */
	public String getTelefono1() {
		return telefono1;
	}

	/**
	 * @param telefono1 the telefono1 to set
	 */
	public void setTelefono1(String telefono1) {
		this.telefono1 = telefono1;
	}

	/**
	 * @return the telefono2
	 */
	public String getTelefono2() {
		return telefono2;
	}

	/**
	 * @param telefono2 the telefono2 to set
	 */
	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

    

}
