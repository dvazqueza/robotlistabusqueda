package com.creditoparati.robotlistabusqueda.web.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.creditoparati.robotlistabusqueda.middleware.services.error.FetchExpedientesErrorException;
import com.creditoparati.robotlistabusqueda.middleware.services.RecursosService;
import com.creditoparati.robotlistabusqueda.middleware.services.HttpService;
import com.creditoparati.robotlistabusqueda.middleware.services.RobotService;
import com.creditoparati.robotlistabusqueda.middleware.services.util.DatosRegresoDTO;
import com.creditoparati.robotlistabusqueda.middleware.services.util.HttpServiceDTO;
import com.creditoparati.robotlistabusqueda.middleware.services.util.UtileriaController;
import com.creditoparati.robotlistabusqueda.model.Bitacora;

@Controller
public class RobotController {

    /**
     * Logging.
     */
    private static final Logger log = Logger.getLogger(RobotController.class);

	@Autowired
	private RobotService robotService;

    @Autowired
    private RecursosService recursosServiceCtlr;

    @Autowired
    private HttpService httpService;

	@RequestMapping(value="/lanzarrobot", method=RequestMethod.POST)
	@ResponseBody
	public String lanzarRobot(@RequestParam("usuario") String usuario,
	                          @RequestParam("passwd") String passwd,
	                          @RequestParam("ntoken") String ntoken) {
	    log.info("######### lanzarRobot()...");

        log.debug("######### usuario=" + usuario);
        log.debug("######### passwd=" + passwd);
        log.debug("######### ntoken=" + ntoken);

        String salida = UtileriaController.armaJSONRespuestaLanzarRobot("falla", 0, "Arrancando el inicio del lanzamiento.");
        HttpServiceDTO httpServ = new HttpServiceDTO();

        int totalExpedientes = 0;
        try {
            totalExpedientes = this.getRecursosServiceCtlr().getTotalExpedientes();
            log.debug("######### Total de expedientes a procesar=" + totalExpedientes);

            if (totalExpedientes == 0) {
                salida = UtileriaController.armaJSONRespuestaLanzarRobot("exito", totalExpedientes, "noexistencreditos");
            }
            else {
//                // SE REALIZA LA AUTENTICACION AL PORTAL DE FOVISSSTE.
//                String errorAutentificacion = "";
//                try {
//                    httpserv = this.getHttpServiceCtlr().autentificarAntesProcesar(usuario, passwd, ntoken);
//                    if (httpserv != null && httpserv.getError().length() > 0) {
//                        errorAutentificacion = httpserv.getError();
//                    }
//                } catch (Exception e) {
//                    errorAutentificacion = "Al tratar de autentificarse al portal de FOVISSSTE.";
//                    e.printStackTrace();
//                }

//                if (errorAutentificacion.length() > 0) {
//                    salida = UtileriaController.armaJSONRespuestaLanzarRobot("falla", totalExpedientes, errorAutentificacion);
//                }
//                else {
            	
            	httpServ.setUsuario(usuario);
            	httpServ.setPasswd(passwd);
            	httpServ.setNtoken(ntoken);
            	salida = UtileriaController.armaJSONRespuestaLanzarRobot("exito", totalExpedientes, "");

                DatosRegresoDTO dr = robotService.inicializaRobot(httpServ);
                if (dr.isValido()) {
                    salida = UtileriaController.armaJSONRespuestaLanzarRobot("exito", totalExpedientes, "");
                }
                else {
                    salida = UtileriaController.armaJSONRespuestaLanzarRobot("falla", totalExpedientes, dr.getError());
                }

            }
        } catch (FetchExpedientesErrorException feee) {
        	salida = UtileriaController.armaJSONRespuestaLanzarRobot("falla", totalExpedientes, "errorobtenercreditos");
        	feee.printStackTrace();
        } catch (Exception e) {
        	salida = UtileriaController.armaJSONRespuestaLanzarRobot("falla", totalExpedientes, e.getMessage());
            e.printStackTrace();
        }

	    log.info("######### Salida Lanzamiento Robot:" + salida);
		return salida;
	}

    @RequestMapping(value="/pollingproceso", method=RequestMethod.POST)
    @ResponseBody
    public String pollingProceso(@RequestParam("tiempo") String tiempo) {
        log.info("######### pollingProceso()...");

        String salida = UtileriaController.armaJSONRespuestaPolling("falla", 0, "Arrancando el polling.");

        DatosRegresoDTO dr = robotService.pollingBusquedaRecursos(tiempo);
        if (dr.isValido()) {
            int procesados = Integer.parseInt(dr.getDatos().get(0));
            salida = UtileriaController.armaJSONRespuestaPolling("exito", procesados, "");
        }
        else {
            salida = UtileriaController.armaJSONRespuestaLanzarRobot("falla", 0, dr.getError());
        }
        log.info("######### Salida Polling Proceso:" + salida);
        return salida;
    }

    @RequestMapping(value="/reporte", method=RequestMethod.POST)
    @ResponseBody
    public String getReporteProcesamiento(@RequestParam("tiempo") String tiempo) {
        log.info("######### getReporteProcesamiento()...");

        String salida = "";
        List<Bitacora> bitacoras = null;

        bitacoras = robotService.getReporteListaBusqueda(tiempo);

        salida = UtileriaController.armaJSONRespuestaReporte(bitacoras);
        log.info("######### Salida de reporte erroneo:" + salida);
        return salida;
//        return rp.toString();
    }

    public RecursosService getRecursosServiceCtlr() {
        return recursosServiceCtlr;
    }

    public HttpService getHttpService() {
        return httpService;
    }

}
