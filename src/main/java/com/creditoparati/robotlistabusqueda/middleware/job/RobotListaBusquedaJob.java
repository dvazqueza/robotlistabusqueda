package com.creditoparati.robotlistabusqueda.middleware.job;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;

import com.creditoparati.robotlistabusqueda.middleware.services.ListaBusquedaService;
import com.creditoparati.robotlistabusqueda.middleware.services.ServiceController;
import com.creditoparati.robotlistabusqueda.middleware.services.util.HttpServiceDTO;

/**
 * Job de LIsta de Busqueda.
 * @author Edgar Deloera
 * @author Daniel Vazquez
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class RobotListaBusquedaJob implements Job {

    /**
     * Log de clase.
     */
    private static final Logger log = Logger.getLogger(RobotListaBusquedaJob.class);

    private ListaBusquedaService listaBusquedaService;

    /**
     * Variables para mantener estado en el Job de Quartz.
     */
    public static final String HTTPSERVICEDTO = "httpservicedto";

    public RobotListaBusquedaJob() {
    }

    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("######### Ejecutando job...");

        // Obteniendo el servicio PagaCreditosService.
        this.obtenListaBusquedaService();
        
        // Se obtienen los datos de estado del Job de Quartz.
        JobDataMap data = context.getJobDetail().getJobDataMap();
        HttpServiceDTO httpServiceDTO = (HttpServiceDTO) data.get(RobotListaBusquedaJob.HTTPSERVICEDTO);

        try {
            log.debug("######### antes de procesoListaBusqueda()...");
            this.getListaBusquedaService().procesoListaBusqueda(httpServiceDTO);
            log.debug("######### termino procesoListaBusqueda()...");
        } catch (Exception e) {
        	log.error("######### Se detecto una Excepcion en procesoListaBusqueda()");
            e.printStackTrace();
            JobExecutionException jee = new JobExecutionException(e);
            jee.setUnscheduleFiringTrigger(true);
            throw jee;
        }
    }

    /**
     * Obtiene el servicio PagoCreditosService del contexto de spring.
     */
    private void obtenListaBusquedaService() {
        if (this.getListaBusquedaService() == null) {
            this.setListaBusquedaService((ListaBusquedaService) ServiceController.getService("listaBusquedaService"));
        }
    }

    public ListaBusquedaService getListaBusquedaService() {
        return listaBusquedaService;
    }

    public void setListaBusquedaService(ListaBusquedaService listaBusquedaService) {
        this.listaBusquedaService = listaBusquedaService;
    }

}
