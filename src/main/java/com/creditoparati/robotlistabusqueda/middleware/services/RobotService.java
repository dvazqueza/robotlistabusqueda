package com.creditoparati.robotlistabusqueda.middleware.services;

import java.util.List;

import com.creditoparati.robotlistabusqueda.middleware.services.util.DatosRegresoDTO;
import com.creditoparati.robotlistabusqueda.middleware.services.util.HttpServiceDTO;
import com.creditoparati.robotlistabusqueda.model.Bitacora;

/**
 * Interface del servicio para el robot.
 * @author Edgar Deloera
 * @author Daniel Vazquez
 */
public interface RobotService {

    /**
     * Inicializa el Robot a demanda.
     * @param httpServiceDTO El DTO con informacion util para el servicio http.
     * @return Resultado de la inicializacion.
     */
    DatosRegresoDTO inicializaRobot(HttpServiceDTO httpServiceDTO);

    /**
     * Obtiene el total de creditos procesados.
     * @param tiempo La cadena de tiempo para empezar la busqueda en la bitacora.
     * @return El numero de creditos procesados.
     */
    DatosRegresoDTO pollingBusquedaRecursos(String tiempo);
    
    /**
     * Obtiene el reporte de recursos obtenirdos de la lista.
     * @param tiempo La cadena de tiempo para empezar la busqueda en la bitacora.
     * @return El listado de rcursos obtenidos satisfactoriamente.
     */
    List<Bitacora> getReporteListaBusqueda(String tiempo);

}
