package com.creditoparati.robotlistabusqueda.middleware.services.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.creditoparati.robotlistabusqueda.middleware.services.error.FetchExpedientesErrorException;
import com.creditoparati.robotlistabusqueda.middleware.services.RecursosService;
import com.creditoparati.robotlistabusqueda.middleware.services.ServiceController;
import com.creditoparati.robotlistabusqueda.model.Expediente;
import com.creditoparati.robotlistabusqueda.model.dao.ExpedienteDAO;
import com.creditoparati.robotlistabusqueda.middleware.services.BitacoraService;
import com.creditoparati.robotlistabusqueda.middleware.services.HttpService;

public class RecursosServiceImpl implements RecursosService {

    /**
     * Log de la clase
     */
    private static final Logger log = Logger.getLogger(RecursosServiceImpl.class);

    /**
     * Servicio de creditos.
     */
    private BitacoraService bitacoraService;

    /**
     * Servicio de HTTP.
     */
    private HttpService httpService;

    /**
     * Propiedad inyectada que indica si se tiene que realizar la etapa cero que es:
     * la VALIDACION DE INFORMACION COMPLETA. 
     */
    private String etapaCero;

    /**
     * Propiedad inyectada que indica cuantos reintentos se deben hacer del envio. 
     */
    private String reintentos;

    
    /**
     * DAO de Creditos.
     */
    private ExpedienteDAO expedienteDao;

    @Override
    public List<Expediente> getExpedientes() throws Exception {
        log.debug("######### Obteniendo listado de creditos...");
        List<Expediente> expedientes = this.obtenExpedienteDao().loadAll();
        return expedientes;
    }

    @Override
    public int getTotalExpedientes() throws FetchExpedientesErrorException {
        log.debug("######### Obteniendo el total de creditos para lanzar el robot ...");
        return this.obtenExpedienteDao().getTotalExpedientes();
    }

    private ExpedienteDAO obtenExpedienteDao() {
        if (this.getExpedienteDao() == null) {
            this.setExpedienteDao((ExpedienteDAO) ServiceController.getService("expedienteDAO"));
        }
        return this.getExpedienteDao();
    }

    public ExpedienteDAO getExpedienteDao() {
        return expedienteDao;
    }

    public void setExpedienteDao(ExpedienteDAO creditosDao) {
        this.expedienteDao = creditosDao;
    }

    public String getEtapaCero() {
        return etapaCero;
    }

    public void setEtapaCero(String etapaCero) {
        this.etapaCero = etapaCero;
    }

    public HttpService getHttpService() {
        return httpService;
    }

    public void setHttpService(HttpService httpService) {
        this.httpService = httpService;
    }

    public String getReintentos() {
        return reintentos;
    }

    public void setReintentos(String reintentos) {
        this.reintentos = reintentos;
    }

    public BitacoraService getBitacoraService() {
        // Obteniendo bean del Servicio de Bitacoreo.
        if (this.bitacoraService == null) {
            this.bitacoraService = (BitacoraService) ServiceController.getService("bitacoraService");
        }
        return this.bitacoraService;
    }

    public void setBitacoraService(BitacoraService bitacoraService) {
        this.bitacoraService = bitacoraService;
    }
}
