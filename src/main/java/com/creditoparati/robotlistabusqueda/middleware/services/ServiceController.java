package com.creditoparati.robotlistabusqueda.middleware.services;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ServiceController {

    /**
     * Log de clase.
     */
    private static final Logger log = Logger.getLogger(ServiceController.class);

	private static final String SPRING_CONTEXT_FILE = "classpath*:/service-context.xml"; 

	private static ClassPathXmlApplicationContext springContext = null; 

	/**
	 * Obtiene un Servicio utilizando SPRING
	 * 
	 * @param String Nombre del Servicio
	 * @return Object Objeto que contiene los Servicios Solicitados.
	 */
	public static Object getService(String service) {
        if (springContext == null) {
            log.info("######### Cargando contexto ...");
            springContext = new ClassPathXmlApplicationContext(SPRING_CONTEXT_FILE);
        }
        return springContext.getBean(service);
	}

}
