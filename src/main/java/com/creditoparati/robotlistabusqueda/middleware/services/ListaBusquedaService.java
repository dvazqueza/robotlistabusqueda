package com.creditoparati.robotlistabusqueda.middleware.services;

import com.creditoparati.robotlistabusqueda.middleware.services.util.HttpServiceDTO;

/**
 * Interfaz del servicio que realiza la busqueda de recursos en lista.
 * @author Edgar Deloera
 * @author Daniel Vazquez
 */
public interface ListaBusquedaService {

    /**
     * Lanza el proceso de busqueda de recursos en lista.
     * @param httpServiceDTO El DTO con objetos utiles para el servicio http.
     * @throws Exception Exception En caso de error.
     */
	public void procesoListaBusqueda(HttpServiceDTO httpServiceDTO) throws Exception;

}
