package com.creditoparati.robotlistabusqueda.middleware.services.impl;

import java.util.List;
import java.util.Vector;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.log4j.Logger;

import com.creditoparati.robotlistabusqueda.middleware.services.ValidadorInformacionService;
import com.creditoparati.robotlistabusqueda.middleware.services.util.DatosRegresoDTO;
import com.creditoparati.robotlistabusqueda.model.Expediente;

public class ValidadorInformacionServiceImpl implements ValidadorInformacionService {

    /**
     * Log de la clase
     */
    private static final Logger log = Logger.getLogger(ValidadorInformacionServiceImpl.class);

    @Override
    public DatosRegresoDTO informacionExpedienteValida(Expediente expediente) throws Exception {
        log.debug("######### Validando datos validos del credito...");
        List<String> emailsNoValidos = null;
        List<String> bancoClabeNoValidos = null;
        String descsErrores = "";
        DatosRegresoDTO dr = new DatosRegresoDTO();

        if (descsErrores.length() == 0) {
            dr.setValido(true);
        }
        else {
            dr.setValido(false);
            dr.setError(descsErrores);
        }

        return dr;
    }

    @Override
    public DatosRegresoDTO informacionExpedienteCompleta(Expediente expediente) throws Exception {
        log.debug("######### Validando datos completos del expediente...");
        DatosRegresoDTO dr = new DatosRegresoDTO();

        List<String> datosFaltantes = null;

        // Validacion de informacion en el credito.
        datosFaltantes = this.validaExistenciaInfo(expediente);

        dr.setDatos(datosFaltantes);
        if (datosFaltantes.size() == 0) {
            // Los datos estan completos
            dr.setValido(true);
        }
        else {
            // Los datos estan incompletos
            log.info("######### Informacion incompleta en expediente:" + expediente.getNumeroSolicitud());
            dr.setValido(false);
        }
        return dr;
    }

    /**
     * Validacion de la existencia de informacion del expediente.
     * @param credito El expediente a validar su informacion.
     * @return Un listado de datos faltantes si existieron.
     * @throws Exception
     */
    private List<String> validaExistenciaInfo(Expediente expediente) {
        List<String> datosFaltantes = new Vector<String>();

        if (expediente.getNombre() == null || expediente.getNombre().equals("")) {
            datosFaltantes.add("Nombre del Beneficiario");
        }
        if (expediente.getNumeroCliente() == null || expediente.getNumeroCliente().equals("")) {
            datosFaltantes.add("Numero Cliente");
        }
        if (expediente.getNumeroSolicitud() == null || expediente.getNumeroSolicitud().equals("")) {
            datosFaltantes.add("No. de Solicitud");
        }
        return datosFaltantes;
    }

}
