package com.creditoparati.robotlistabusqueda.middleware.services.util;

import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.protocol.HttpContext;

public class HttpServiceDTO {

    HttpClient clienteHttp = null;
    HttpResponse respHttp = null;
    HttpContext contexto = null;
    String usuario = null;
    String passwd = null;
    String ntoken = null;
    String viewState = null;
    String toolkitSM = null;
    String error = null;
    String expediente = null;
    String idExpediente = null;
    String fechaAutorizacion = null;
    String fechaVencimiento = null;
    String etapaNotaria = null;
    Map<String, String> camposForma;

    public HttpServiceDTO() {
    }

    public HttpServiceDTO(HttpClient clienteHttp, HttpResponse respHttp, HttpContext contexto, 
                          String viewState, String toolkitSM, String error, String expediente) {
        this.setClienteHttp(clienteHttp);
        this.setRespHttp(respHttp);
        this.setContexto(contexto);
        this.setViewState(viewState);
        this.setToolkitSM(toolkitSM);
        this.setError(error);
        this.setExpediente(expediente);
    }

    public HttpClient getClienteHttp() {
        return clienteHttp;
    }
    public void setClienteHttp(HttpClient clienteHttp) {
        this.clienteHttp = clienteHttp;
    }
    public HttpResponse getRespHttp() {
        return respHttp;
    }
    public void setRespHttp(HttpResponse respHttp) {
        this.respHttp = respHttp;
    }
    public HttpContext getContexto() {
        return contexto;
    }
    public void setContexto(HttpContext contexto) {
        this.contexto = contexto;
    }
    public String getViewState() {
        return viewState;
    }
    public void setViewState(String viewState) {
        this.viewState = viewState;
    }
    public String getToolkitSM() {
        return toolkitSM;
    }
    public void setToolkitSM(String toolkitSM) {
        this.toolkitSM = toolkitSM;
    }
    public String getError() {
        return error;
    }
    public void setError(String error) {
        this.error = error;
    }
    public String getExpediente() {
        return expediente;
    }
    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

	public String getIdExpediente() {
		return idExpediente;
	}

	public void setIdExpediente(String idExpediente) {
		this.idExpediente = idExpediente;
	}

	public String getFechaAutorizacion() {
		return fechaAutorizacion;
	}
 
    public void setFechaAutorizacion(String fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
 
    public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getEtapaNotaria() {
		return etapaNotaria;
	}

	public void setEtapaNotaria(String etapaNotaria) {
		this.etapaNotaria = etapaNotaria;
	}

	/**
	 * @return the camposForma
	 */
	public Map<String, String> getCamposForma() {
		return camposForma;
	}

	/**
	 * @param camposForma the camposForma to set
	 */
	public void setCamposForma(Map<String, String> camposForma) {
		this.camposForma = camposForma;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the passwd
	 */
	public String getPasswd() {
		return passwd;
	}

	/**
	 * @param passwd the passwd to set
	 */
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	/**
	 * @return the ntoken
	 */
	public String getNtoken() {
		return ntoken;
	}

	/**
	 * @param ntoken the ntoken to set
	 */
	public void setNtoken(String ntoken) {
		this.ntoken = ntoken;
	}

}
