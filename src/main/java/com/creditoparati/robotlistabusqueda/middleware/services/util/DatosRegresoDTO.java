package com.creditoparati.robotlistabusqueda.middleware.services.util;

import java.util.List;
import java.util.Vector;

public class DatosRegresoDTO {

    boolean valido = false;
    List<String> datos = new Vector<String>();
    String error = "";

    public DatosRegresoDTO() {
    }

    public boolean isValido() {
        return valido;
    }
    public void setValido(boolean valido) {
        this.valido = valido;
    }
    public List<String> getDatos() {
        return datos;
    }
    public void setDatos(List<String> datos) {
        this.datos = datos;
    }
    public String getError() {
        return error;
    }
    public void setError(String error) {
        this.error = error;
    }

}
