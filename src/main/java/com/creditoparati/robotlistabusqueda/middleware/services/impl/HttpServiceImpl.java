package com.creditoparati.robotlistabusqueda.middleware.services.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.log4j.Logger;

import com.creditoparati.robotlistabusqueda.middleware.services.HttpService;
import com.creditoparati.robotlistabusqueda.middleware.services.error.RobotLoginException;
import com.creditoparati.robotlistabusqueda.middleware.services.util.DatosRegresoDTO;
import com.creditoparati.robotlistabusqueda.middleware.services.util.HttpServiceDTO;
import com.creditoparati.robotlistabusqueda.model.Expediente;

public class HttpServiceImpl implements HttpService {

    /**
     * Log de la clase
     */
    private static final Logger log = Logger.getLogger(HttpServiceImpl.class);

    /**
     * LLaves para el mapa de Fechas
     */
	private final String[] CAMPOS_FORMA_VERIFICACION= {
			"TipoAhorroHiddenField"           ,
			"JubiSubHiddenField"               ,
			"SaldoSarHiddenField"              ,
			"PnlsubsidiosJubHiddenField"       ,
			"SARInfonavitHiddenField"          ,
			"MontoCreditoInfonavitHiddenField" ,
			"ValorAvaluoHiddenField"           ,
			"hdd1SalarioEnPesos"               ,
			"hddAhorroPrevio"                  ,
			"CURPHiddenField"                  ,
			"CURPCONYHiddenField"              ,
			"ID_LineaCreditoHiddenField"       ,
			"TopeMontoAliadosHiddenField"      ,
			"AplicaSubsidioHiddenField"        ,
			"EsJubiladoHiddenField"            ,
			"HiddenFieldPensionAlimenticia"    ,
			"HiddenFieldRegresarFaseXPension"  ,
			"HiddenFieldNombreUsuario"         };


    /**
     * Propiedad inyectada que indica el url de login de Fovissste. 
     */
    private String urlLogin;

    /**
     * Propiedad inyectada que indica el url de busqueda de un expediente. 
     */
    private String urlBusqueda;

    /**
     * Propiedad inyectada que indica el url de envio del pago a beneficiario. 
     */
    private String urlAvanzaEtapa;
    private String urlAvanzaEtapaHost;
    private String urlAvanzaEtapaScheme;
    private String urlAvanzaEtapaPath;
    

    /**
     * Propiedad inyectada que indica el url de envio del pago a beneficiario. 
     */
    private String urlVerificacionFinal;

    /**
     * Propiedad inyectada que indica el urpara avanzar a Notaria. 
     */
    private String urlAvanzaNotaria;

    /**
     * Propiedad inyectada que indica el url de log out. 
     */
    private String urlLogout;

    /**
     * Propiedad inyectada que indica el url de inicio del portal. 
     */
    private String urlInicio;
    /**
     * Propiedad inyectada que indica el nombre del parametro de usuario. 
     */
    private String paramUsuario;

    /**
     * Propiedad inyectada que indica el nombre del parametro de password. 
     */
    private String paramPassword;

    /**
     * Propiedad inyectada que indica el nombre del parametro del token. 
     */
    private String paramToken;

    /**
     * Propiedad inyectada que indica el uso del token al loguearse. 
     */
    private String usaToken;


    @Override
    public DatosRegresoDTO enviaExpediente(Expediente expediente, HttpServiceDTO httpServiceDTO) throws Exception {
        // Se realiza el envio de los datos de Expedientes.
        DatosRegresoDTO dr = new DatosRegresoDTO();

        // Se obtiene el expediente y otros elementos mediante una busqueda.
        httpServiceDTO = this.obtenExpedienteConBusqueda(expediente, httpServiceDTO);
        if (httpServiceDTO.getIdExpediente() != null) {
        	httpServiceDTO = this.obtenerDisponibilidadRecursos(httpServiceDTO);
        	if (httpServiceDTO.getEtapaNotaria() != null && !"".equals(httpServiceDTO.getEtapaNotaria())) {
            	dr.setValido(false);
                dr.setError("El expediente [" +  expediente.getNumeroSolicitud() + "] contiene el mensaje :[" + httpServiceDTO.getEtapaNotaria() + "]");
        	}
        	else if (httpServiceDTO.getFechaAutorizacion() != null && !"".equals(httpServiceDTO.getFechaAutorizacion())) {
            	httpServiceDTO = this.avanzarExpedienteNotaria(httpServiceDTO);
                if (httpServiceDTO.getError() != null && httpServiceDTO.getError().length() > 0) {
                	dr.setValido(false);
                    dr.setError("No se puede avanzar el expediente a Notaria [" +  expediente.getNumeroSolicitud() + "]");
                }
                else {
                	// TODO: dr = this.revisaExpediente(expediente, httpServiceDTO);
                	dr.setValido(true);
                    dr.setError("");
                }
            }
            else if(httpServiceDTO.getFechaVencimiento() != null && !"".equals(httpServiceDTO.getFechaVencimiento())){
            	httpServiceDTO = this.avanzarExpedienteNotaria(httpServiceDTO);
                if (httpServiceDTO.getError() != null && httpServiceDTO.getError().length() > 0) {
                	dr.setValido(false);
                    dr.setError("No se puede avanzar el expediente a Notaria [" +  expediente.getNumeroSolicitud() + "]");
                }
                else {
                	// TODO: dr = this.revisaExpediente(expediente, httpServiceDTO);
                	dr.setValido(true);
                    dr.setError("");
                }
            }
            else {
                    dr.setValido(false);
                    dr.setError("No existen recursos para expediente [" + expediente.getNumeroSolicitud() + "]");
            }
        }
        else {
            dr.setValido(false);
            dr.setError("No se pudo encontrar el expediente para [" + expediente.getNumeroSolicitud() + "]");
        }

        return dr;
    }
   

    @Override
    public DatosRegresoDTO autentificarAntesProcesar(HttpServiceDTO httpserv) throws RobotLoginException {
        
    	DatosRegresoDTO dr;
		String cadenaError;
		boolean loginValido = false;
		try {
			log.info("######### Autenticando ...");
			dr = new DatosRegresoDTO();
			cadenaError = "";

			// Se realiza http get a la pagina de login para obtener el __VIEWSTATE 
			String viewState = this.obtenViewStateConHttpGet();
			String toolkitSM = ";;AjaxControlToolkit, Version=1.0.20229.32475, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e:en-US:f6225dc9-7526-492d-a359-be5dc356a179:e2e86ef9:1df13a87:fde3863c";

			// Realizando autentificacion con usuario y password.
			List<NameValuePair> formlogin = new ArrayList<NameValuePair>();
			formlogin.add(new BasicNameValuePair("ButtonLogin", ":: Aceptar ::"));
			formlogin.add(new BasicNameValuePair(this.getParamPassword(), httpserv.getPasswd()));
			formlogin.add(new BasicNameValuePair(this.getParamUsuario(), httpserv.getUsuario()));
			formlogin.add(new BasicNameValuePair("__LASTFOCUS", ""));
			formlogin.add(new BasicNameValuePair("ToolkitScriptManager1_HiddenField", toolkitSM));
			formlogin.add(new BasicNameValuePair("__VIEWSTATE", viewState));

			UrlEncodedFormEntity entitylogin = new UrlEncodedFormEntity(formlogin, "UTF-8");
			HttpPost httppostlogin = new HttpPost(this.getUrlLogin());
			httppostlogin.setEntity(entitylogin);
			HttpClient httpclientlogin = new DefaultHttpClient();

			//Create a local instance of cookie store
			CookieStore cookieStore = new BasicCookieStore();
			//Create local HTTP context
			HttpContext localContext = new BasicHttpContext();
			//Bind custom cookie store to the local context
			localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

			log.info("######### haciendo request de autentificacion=" + this.getUrlLogin());
			HttpResponse responselogin = httpclientlogin.execute(httppostlogin, localContext);

			log.info("######### status de login=" + responselogin.getStatusLine().getStatusCode());
			log.info("######### reason de login=" + responselogin.getStatusLine().getReasonPhrase());

			httpserv.setClienteHttp(httpclientlogin);
			httpserv.setRespHttp(responselogin);
			httpserv.setContexto(localContext);
			httpserv.setViewState(viewState);
			httpserv.setToolkitSM(toolkitSM);

			if (responselogin.getStatusLine().getStatusCode() != 200) {
			    httpserv.setError(responselogin.getStatusLine().getReasonPhrase());
			    dr.setError(httpserv.getError());
			    dr.setValido(false);
			    return dr;
			}

			List<Cookie> cookies = cookieStore.getCookies();
			if (cookies.isEmpty()) {
			    log.debug("######### No hay cookies.");
			} else {
			    for (int i = 0; i < cookies.size(); i++) {
			        log.debug("######### COOKIE - " + cookies.get(i).toString());
			    }
			}

			// Si hubo respuesta se comienza el trabajo con el portal.
			HttpEntity responseEntityLogin = responselogin.getEntity();
			if (responseEntityLogin != null) {
			    responseEntityLogin = new BufferedHttpEntity(responseEntityLogin);
			    BufferedReader rd = new BufferedReader(new InputStreamReader(responseEntityLogin.getContent()));

			    // Verifica error al hacer el login y obtiene los campos HiddenFieldIdUsuario y HiddenFieldNoToken.
			    Map<String, String> resVerif = this.verificaErrorAlLogin(rd);

			    if (resVerif.get("error") != null) {
			        cadenaError = resVerif.get("error");
			    }
			    log.debug("######### cadenaError length:" + cadenaError.length());
			    log.debug("######### usatoken:" + this.getUsaToken());
			    // Verificacin del uso del token y enviado al portal de Fovissste.
			    if (cadenaError.length() <= 0 && this.getUsaToken() != null && this.getUsaToken().equals("1")) {
			        log.info("######### Se usara token...");
			        String idUsuario = "";
			        String numToken = "";

			        idUsuario = (resVerif.get("HiddenFieldIdUsuario") != null) ? resVerif.get("HiddenFieldIdUsuario") : "";
			        numToken = (resVerif.get("HiddenFieldNoToken") != null) ? resVerif.get("HiddenFieldNoToken") : "";

			        if (idUsuario.length() > 0 && numToken.length() > 0) {
			            idUsuario = this.obtieneValorIdUsuario(idUsuario);
			            numToken = this.obtieneValorNumToken(numToken);
			            if (idUsuario.length() > 0 && numToken.length() > 0) {
			                // Validacion de token.
			                List<NameValuePair> formtoken = new ArrayList<NameValuePair>();
			                formtoken.add(new BasicNameValuePair("ButtonLoginToken", ":: Aceptar ::"));
			                formtoken.add(new BasicNameValuePair("HiddenFieldBloqueado", "0"));
			                formtoken.add(new BasicNameValuePair("HiddenFieldIdUsuario", idUsuario));
			                formtoken.add(new BasicNameValuePair("HiddenFieldNoToken", numToken));
			                formtoken.add(new BasicNameValuePair("HiddenFieldPrimeraVez", "1"));
			                formtoken.add(new BasicNameValuePair(this.getParamPassword(), httpserv.getNtoken()));
			                formtoken.add(new BasicNameValuePair(this.getParamUsuario(), httpserv.getUsuario()));
			                formtoken.add(new BasicNameValuePair("ToolkitScriptManager1_HiddenField", toolkitSM));
			                formtoken.add(new BasicNameValuePair("__VIEWSTATE", viewState));

			                UrlEncodedFormEntity entitytoken = new UrlEncodedFormEntity(formtoken, "UTF-8");
			                HttpPost httpposttoken = new HttpPost(this.getUrlLogin());
			                httpposttoken.setEntity(entitytoken);

			                log.info("######### haciendo request de validacion token=" + this.getUrlLogin());
			                HttpResponse responsetoken = httpserv.getClienteHttp().execute(httpposttoken, httpserv.getContexto());

			                log.info("######### status de validacion token=" + responsetoken.getStatusLine().getStatusCode());
			                log.info("######### reason de validacion token=" + responsetoken.getStatusLine().getReasonPhrase());
			                if (responsetoken.getStatusLine().getStatusCode() != 200) {
			                    httpserv.setError(responsetoken.getStatusLine().getReasonPhrase());
			                    dr.setError(httpserv.getError());
			                    dr.setValido(false);
			                    return dr;
			                }

			                rd = null;
			                httpserv.setRespHttp(responsetoken);

			                // Busqueda de error en la validacion de token.
			                HttpEntity responseEntityToken = responsetoken.getEntity();
			                if (responseEntityToken != null) {
			                    responseEntityToken = new BufferedHttpEntity(responseEntityToken);
			                    BufferedReader rdtok = new BufferedReader(new InputStreamReader(responseEntityToken.getContent()));

			                    String linet = "";
			                    String lineaTrimeadat = "";

			                    // Verificando si existe error en la pagina HTML, via el elemento LabelError.
			                    try {
			                        while ((linet = rdtok.readLine()) != null) {
			                            lineaTrimeadat = linet.trim();
			                            if (lineaTrimeadat.contains("LabelError")) {
			                                log.debug("######### linea de error al validar token=[" + lineaTrimeadat + "]");
			                                cadenaError = this.obtieneCadenaError(lineaTrimeadat);
			                                break;
			                            }
			                        }
			                    } catch(IOException ioe) {
			                        cadenaError = "Al verificar si hay error al validar el token.";
			                        ioe.printStackTrace();
			                    }

			                    log.debug("######### cadenaError al verificar error login token=[" + cadenaError + "]");
			                    // Validar que se haya hecho login correctamente.
			                    if (cadenaError.length() <= 0) {
			                        if (!this.verificaExitoEnLogin(httpserv)) {
			                            cadenaError = "Al verificar si hubo exito en el login al portal.";
			                        }
			                        else {
						            	loginValido = true;
						            	cadenaError = "";
			                        }
			                    }
			                    rdtok = null;
			                }
			                else {
			                	cadenaError = "No hubo respuesta del challenge del token.";
			                }
			            }
			            else {
			                cadenaError = "No se encontro el elemento HiddenFieldIdUsuario o HiddenFieldNoToken.";
			            }
			        }
			        else {
			            cadenaError = "No se encontro el elemento HiddenFieldIdUsuario o HiddenFieldNoToken.";
			        }
			    }
			    else {
			        // Si entra al else quiere decir que no se esta haciendo uso del token y se tiene 
			        // que validar si hubo exito en el login.
			    	if (cadenaError.length() <= 0) {
			            if (!this.verificaExitoEnLogin(httpserv)) {
			                cadenaError = "Al verificar si hubo exito en el login al portal.";
			            }
			            else {
			            	loginValido = true;
			            	cadenaError = "";
			            }
			        }
			    }

			}
	        httpserv.setError(cadenaError);
	        dr.setError(cadenaError);
	        dr.setValido(loginValido);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RobotLoginException(e);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			throw new RobotLoginException(e);
		} catch (IllegalStateException e) {
			e.printStackTrace();
			throw new RobotLoginException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RobotLoginException(e);
		}
        return dr;
    }

    /**
     * Obtiene el valor del elemento '__VIEWSTATE' haciendo la obtencion de la pagina de login del 
     * portal de fovissste.
     * @return El string con el valor o vacio.
     */
    private String obtenViewStateConHttpGet() {
        String viewState = "";

        try {
            HttpGet httpgetlogin = new HttpGet(this.getUrlLogin());
            HttpClient httpclientlogin = new DefaultHttpClient();

            log.info("######### haciendo request de obtener VIEWSTATE=" + this.getUrlLogin());
            HttpResponse responselogin = httpclientlogin.execute(httpgetlogin);

            log.info("######### status para viewState=" + responselogin.getStatusLine().getStatusCode());
            log.info("######### reason para viewState=" + responselogin.getStatusLine().getReasonPhrase());
            HttpEntity responseEntity = responselogin.getEntity();
            if (responseEntity != null) {
                try {
                    responseEntity = new BufferedHttpEntity(responseEntity);
                    BufferedReader rd = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
                    viewState = this.obtenCampoInput("__VIEWSTATE", rd);
                    if (viewState == null) {
                        viewState = "";
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        } catch (ClientProtocolException cpe) {
            viewState = "";
            cpe.printStackTrace();
        } catch (IOException ioe) {
            viewState = "";
            ioe.printStackTrace();
        }

        return viewState;
    }

    /**
     * Verifica si se pudo hacer el login haciendo un request a la pagina de inicio.
     * @param httpserv Un DTO con datos del cliente http que se uso en el login.
     * @return True si se verifico el exito del login, false en caso contrario.
     */
    private boolean verificaExitoEnLogin(HttpServiceDTO httpserv) {
        boolean exito = false;

        try {
            HttpGet httpgetinicio = new HttpGet(this.getUrlInicio());
//            HttpClient httpclientinicio = new DefaultHttpClient();

            log.info("######### haciendo request de main=" + this.getUrlInicio());
            HttpResponse responseinicio = httpserv.getClienteHttp().execute(httpgetinicio, httpserv.getContexto());

            log.info("######### status para pagina inicio=" + responseinicio.getStatusLine().getStatusCode());
            log.info("######### reason para pagina inicio=" + responseinicio.getStatusLine().getReasonPhrase());
            HttpEntity responseEntity = responseinicio.getEntity();
            if (responseEntity != null) {
                responseEntity = new BufferedHttpEntity(responseEntity);
                BufferedReader rd = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
                exito = this.huboExitoEnLogin(rd);
            }
        } catch (ClientProtocolException cpe) {
            cpe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return exito;
    }

    /**
     * Verifica si el login ha sido exitoso con la busqueda de cierto texto.
     * @param br El buffered reader de donde se validara si hubo exito en el login.
     * @return True en caso de exito, false en caso contrario.
     */
    private boolean huboExitoEnLogin(BufferedReader rd) {
        boolean exito = false;
        String line = "";
        String lineaTrimeada = "";

        // Verificando si existe el action = main.aspx para validar el logueo correcto.
        try {
            while ((line = rd.readLine()) != null) {
                lineaTrimeada = line.trim();
                if (lineaTrimeada.contains("barText='MEN")) {
                    exito = true;
                    break;
                }
            }
        } catch(IOException ioe) {
            exito = false;
            ioe.printStackTrace();
        }
        return exito;
    }

    /**
     * Obtiene el valor del elemento HiddenFieldIdUsuario de un string HTML.
     * @param html La cadena html donde se buscara el valor del elemento.
     * @return El valor del elemento si se encontro.
     */
    private String obtieneValorIdUsuario(String html) {
        String idus = "";
        try {
            if (html.length() > 85) {
                int indValue = 0;
                indValue = html.indexOf("value");
                idus = html.substring(indValue);
                if (idus.startsWith("value")) {
                    indValue = idus.indexOf("\"");
                    idus = idus.substring(indValue + 1);
                    indValue = idus.indexOf("\"");
                    idus = idus.substring(0, indValue);
                }
                else {
                    idus = "";
                }
            }
        } catch(Exception e) {
            idus = "";
            e.printStackTrace();
        }
        log.info("######### Elemento HiddenFieldIdUsuario=[" + idus + "]");
        return idus;
    }

    /**
     * Obtiene el valor del elemento HiddenFieldNoToken de un string HTML.
     * @param html La cadena html donde se buscara el valor del elemento.
     * @return El valor del elemento si se encontro.
     */
    private String obtieneValorNumToken(String html) {
        String notoken = "";
        try {
            if (html.length() > 82) {
                int indValue = 0;
                indValue = html.indexOf("value");
                notoken = html.substring(indValue);
                if (notoken.startsWith("value")) {
                    indValue = notoken.indexOf("\"");
                    notoken = notoken.substring(indValue + 1);
                    indValue = notoken.indexOf("\"");
                    notoken = notoken.substring(0, indValue);
                }
                else {
                    notoken = "";
                }
            }
        } catch(Exception e) {
            notoken = "";
            e.printStackTrace();
        }
        log.info("######### Elemento HiddenFieldNoToken=[" + notoken + "]");
        return notoken;
    }

    /**
     * Se verifica si hubo algun error al momento de realizar el login al portal.
     * @param rd El BufferedReader armado del response del portal.
     * @return El herror lo hubo.
     */
    private Map<String, String> verificaErrorAlLogin(BufferedReader rd) {
        String line = "";
        String lineaTrimeada = "";
        Map<String, String> mapa = new HashMap<String, String>();

        // Verificando si existe error en la pagina HTML, via el elemento LabelError.
        try {
            while ((line = rd.readLine()) != null) {
                lineaTrimeada = line.trim();
                if (lineaTrimeada.contains("HiddenFieldIdUsuario")) {
                    log.debug("######### linea HiddenFieldIdUsuario=[" + lineaTrimeada + "]");
                    mapa.put("HiddenFieldIdUsuario", lineaTrimeada);
//                    break;
                }
                if (lineaTrimeada.contains("HiddenFieldNoToken")) {
                    log.debug("######### linea HiddenFieldNoToken=[" + lineaTrimeada + "]");
                    mapa.put("HiddenFieldNoToken", lineaTrimeada);
//                    break;
                }
                if (lineaTrimeada.contains("LabelError")) {
                    log.debug("######### linea de error en login=[" + lineaTrimeada + "]");
                    mapa.put("error", this.obtieneCadenaError(lineaTrimeada));
//                    break;
                }
            }
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }

        return mapa;
    }

    /**
     * Analiza el string y obtiene la cadena de error.
     * @param html La cadena html con el error.
     * @return El error.
     */
    private String obtieneCadenaError(String html) {
        String error = "";
        try {
            if (html.length() > 70) {
                error = html.substring(72);
                int indMenorque = 0;
                if (error.contains("</")) {
                    indMenorque = error.indexOf("</");
                    error = error.substring(0, indMenorque);
                }
            }
        } catch(Exception e) {
            error = "Al obtener el error de la pagina HTML.";
            e.printStackTrace();
        }
      log.info("######### Cadena del elemento de error=[" + error + "]");
      return error;
    }

    /**
     * Realiza la busqueda del expediente realizando una busqueda en primer lugar y luego realizar
     * el filtrado del elemento ExpedienteHiddenField.
     * @param expediente El expediente a revisar.
     * @param httpServiceDTO El DTO con el objeto HttpClient de la autenticacion.
     * @return El DTO con el expediente asociado.
     */
    private HttpServiceDTO obtenExpedienteConBusqueda(Expediente expediente, HttpServiceDTO httpServiceDTO) {
        // Se realiza la busqueda al portal.
        httpServiceDTO = this.realizaBusqueda(expediente, httpServiceDTO);
        if (httpServiceDTO.getError() != null && httpServiceDTO.getError().length() > 0) {
            httpServiceDTO.setExpediente("");
            return httpServiceDTO;
        }

        HttpEntity responseEntity = httpServiceDTO.getRespHttp().getEntity();
        if (responseEntity != null) {
            try {
                responseEntity = new BufferedHttpEntity(responseEntity);
                BufferedReader rd = new BufferedReader(new InputStreamReader(responseEntity.getContent()));

                // Obteniendo el valor del expediente.
                httpServiceDTO.setIdExpediente(this.obtieneLinkVerificacion(rd));
            } catch (IOException ioe) {
                httpServiceDTO.setIdExpediente(null);
                ioe.printStackTrace();
            }
        }
        return httpServiceDTO;
    }


    @Override
    public DatosRegresoDTO revisionExpediente(Expediente expediente, HttpServiceDTO httpServiceDTO) throws Exception {
        // Se realiza la revision de los datos del expediente.
        return this.revisaExpediente(expediente, httpServiceDTO);
    }

    /**
     * Realiza la revision del expediente en el portal de fovissste mediante una consulta.
     * @param expediente El expediente a revisar.
     * @param httpServiceDTO El DTO con el objeto HttpClient de la autenticacion.
     * @return DTO con la informacion resultante de la revision del expediente.
     */
    private DatosRegresoDTO revisaExpediente(Expediente expediente, HttpServiceDTO httpServiceDTO) {
        DatosRegresoDTO dr = new DatosRegresoDTO();
        httpServiceDTO = this.realizaBusqueda(expediente, httpServiceDTO);

        if (httpServiceDTO.getRespHttp() != null && (httpServiceDTO.getError() == null || httpServiceDTO.getError().length() <= 0)) {
            dr = this.revisaDatosExpediente(expediente, httpServiceDTO.getRespHttp());
        }
        else {
            dr.setValido(false);
            dr.setError("Al realizar la busqueda del expediente en el portal.");
        }

        return dr;
    }

    /**
     * Realiza la revision de los datos uno a uno del expediente con los datos que la busqueda retorno.
     * @param expediente El expediente original.
     * @param response El reponse que el portal de fovissste retorno con los datos de la consulta.
     * @return Un DTO con el resultado de la revision.
     */
    private DatosRegresoDTO revisaDatosExpediente(Expediente expediente, HttpResponse response) {
        DatosRegresoDTO dr = new DatosRegresoDTO();

        HttpEntity responseEntity = response.getEntity();
        if (responseEntity != null) {
            try {
                List<String> datosErroneos = new Vector<String>();
                responseEntity = new BufferedHttpEntity(responseEntity);
                BufferedReader rd = new BufferedReader(new InputStreamReader(responseEntity.getContent()));

                String nombre = this.obtenCampoInput("NombreTextBox", rd);
                String numCliente = this.obtenCampoInput("NumeroClienteTextBox", rd);
                String numSolicitud = this.obtenCampoInput("NumeroSolicitudTextBox", rd);

                if (!nombre.equals((expediente.getNombre() != null) ? expediente.getNombre().toUpperCase() : "")) {
                    datosErroneos.add("Nombre del beneficiario");
                }
                if (!numCliente.equals(expediente.getNumeroCliente())) {
                    datosErroneos.add("Numero de cuenta");
                }
                if (!numSolicitud.equals(expediente.getNumeroSolicitud())) {
                    datosErroneos.add("Numero de cuenta");
                }

                if (datosErroneos.size() > 0) {
                    dr.setValido(false);
                    dr.setDatos(datosErroneos);
                }
                else {
                    dr.setValido(true);
                }
                
            } catch (IOException ioe) {
                dr.setValido(false);
                dr.setError(ioe.getMessage());
                ioe.printStackTrace();
            } catch (Exception e) {
                dr.setValido(false);
                dr.setError(e.getMessage());
                e.printStackTrace();
            }
        }

        return dr;
    }

    /**
     * Obtiene el valor del campo pedido por el parametro campo (elemento radio), realiza la busqueda del parametro 
     * en el BufferedReader.
     * @param campo El campo del cual se necesita el valor.
     * @param rd El BufferedReader del cual se buscara el campo pedido.
     * @return El valor si se encuentra o null en caso contrario.
     */
    private String obtenCampoRadio(String campo, BufferedReader rd) {
        String line = "";
        String lineaTrimeada = "";
        String valor = "";

        try {
            while ((line = rd.readLine()) != null) {
                lineaTrimeada = line.trim();
                if (lineaTrimeada.contains(campo)) {
                    if (lineaTrimeada.contains("value")) {
                        if (lineaTrimeada.contains("<input")) {
                            line = lineaTrimeada.substring(lineaTrimeada.indexOf("<input"), lineaTrimeada.indexOf("/>"));
                            if (line.contains("value=\"Si\"") && line.contains("checked=\"checked\"")) {
                                valor = "Si";
                            }
                            else {
                                valor = "No";
                            }
                        }
                        break;
                    }
                }
            }
        } catch (Exception e) {
            valor = "";
            e.printStackTrace();
        }
        return valor;
    }

    
    /**
     * Busca en los datos de verificación si existe la fecha y la regresa
     * @param rd El BufferedReader del cual se buscara el campo pedido.
     * @return el valos si se encuentra o null en caso contrario
     */
    private String obtenFechaVencimiento(BufferedReader rd) {
    	
        final String seekLabel = "FechaVencimientoDatoLabel";
        String line = "";
        String lineaTrimeada = "";
        String valor = "";

        try {
            int indValue = 0;
            while ((line = rd.readLine()) != null) {
                lineaTrimeada = line.trim();
                if (lineaTrimeada.contains(seekLabel)) {
                    indValue = lineaTrimeada.indexOf(">");
                    if (indValue > 0) {
                        valor = lineaTrimeada.substring(indValue + 1);
                        if (valor.contains("</span>")) {
                            indValue = valor.indexOf("</span>");
                            valor = valor.substring(0, indValue);
                        }
                    }
                    break;
                }
            }
        } catch (IOException e) {
            valor = "";
            e.printStackTrace();
        }
    	    	
    	return valor.trim();
    }

    /**
     * Busca en los datos de verificación si existe un mensaje de notaria y la regresa
     * @param rd El BufferedReader del cual se buscara el campo pedido.
     * @return el valos si se encuentra o null en caso contrario
     */
    private String obtenNotariaMensaje(BufferedReader rd) {
    	
        final String seekLabel = "lblAutorizaCambio";
        String line = "";
        String lineaTrimeada = "";
        String valor = "";

        try {
            int indValue = 0;
            while ((line = rd.readLine()) != null) {
                lineaTrimeada = line.trim();
                if (lineaTrimeada.contains(seekLabel)) {
                    indValue = lineaTrimeada.indexOf(">");
                    if (indValue > 0) {
                        valor = lineaTrimeada.substring(indValue + 1);
                        if (valor.contains("</span>")) {
                            indValue = valor.indexOf("</span>");
                            valor = valor.substring(0, indValue);
                        }
                    }
                    break;
                }
            }
        } catch (IOException e) {
            valor = "";
            e.printStackTrace();
        }
    	return valor.trim();
    }
    
    /**
     * Busca en los datos de verificación si existe la fecha y la regresa
     * @param rd El BufferedReader del cual se buscara el campo pedido.
     * @return el valos si se encuentra o null en caso contrario
     */
    private String obtenFechaAutorizacionFirma (BufferedReader rd) {
    	
        final String seekLabel = "FechaAutorizacionFirmaDatoLabel";
        String line = "";
        String lineaTrimeada = "";
        String valor = "";

        try {
            int indValue = 0;
            while ((line = rd.readLine()) != null) {
                lineaTrimeada = line.trim();
                if (lineaTrimeada.contains(seekLabel)) {
                    indValue = lineaTrimeada.indexOf(">");
                    if (indValue > 0) {
                        valor = lineaTrimeada.substring(indValue + 1);
                        if (valor.contains("</span>")) {
                            indValue = valor.indexOf("</span>");
                            valor = valor.substring(0, indValue);
                        }
                    }
                    break;
                }
            }
        } catch (IOException e) {
            valor = "";
            e.printStackTrace();
        }
    	    	
    	return valor.trim();
    }
    
    /**
     * Obtiene el valor del campo pedido por el parametro campo (elemento textarea), realiza la busqueda del parametro 
     * en el BufferedReader.
     * @param campo El campo del cual se necesita el valor.
     * @param rd El BufferedReader del cual se buscara el campo pedido.
     * @return El valor si se encuentra o null en caso contrario.
     */
    private String obtenCampoTextArea(String campo, BufferedReader rd) {
        String line = "";
        String lineaTrimeada = "";
        String valor = "";

        try {
            int indValue = 0;
            while ((line = rd.readLine()) != null) {
                lineaTrimeada = line.trim();
                if (lineaTrimeada.contains(campo)) {
                    indValue = lineaTrimeada.indexOf(">");
                    if (indValue > 0) {
                        valor = lineaTrimeada.substring(indValue + 1);
                        if (valor.contains("</textarea>")) {
                            indValue = valor.indexOf("</textarea>");
                            valor = valor.substring(0, indValue);
                        }
                    }
                    break;
                }
            }
        } catch (IOException e) {
            valor = "";
            e.printStackTrace();
        }
        return valor;
    }

    /**
     * Obtiene el valor del campo pedido por el parametro campo (elemento input), realiza la busqueda del parametro 
     * en el BufferedReader.
     * @param campo El campo del cual se necesita el valor.
     * @param rd El BufferedReader del cual se buscara el campo pedido.
     * @return El valor si se encuentra o null en caso contrario.
     */
    private String obtieneLinkVerificacion(BufferedReader rd) {
        String line = "";
        String lineaTrimeada = "";
        String valor = "";
        String cadenaVerificacion = "ID_Fase=6";

        try {
            int indValue = 0;
            while ((line = rd.readLine()) != null) {
                lineaTrimeada = line.trim();
                if (lineaTrimeada.contains(cadenaVerificacion)) {
                    indValue = lineaTrimeada.indexOf("ID_Expediente");
                    if (indValue > 0) {
                        valor = lineaTrimeada.substring(indValue);
                        if (valor.startsWith("ID_Expediente")) {
                            indValue = valor.indexOf("=");
                            valor = valor.substring(indValue + 1);
                            indValue = valor.indexOf("&");
                            valor = valor.substring(0, indValue);
                            break;
                        }
                        else {
                            valor = "";
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            valor = "";
            e.printStackTrace();
        }
        log.info("######### ID_Expediente: = [" + valor + "]" );
        return valor;
    }
    
    
    /**
     * Obtiene el valor del campo pedido por el parametro campo (elemento input), realiza la busqueda del parametro 
     * en el BufferedReader.
     * @param campo El campo del cual se necesita el valor.
     * @param rd El BufferedReader del cual se buscara el campo pedido.
     * @return El valor si se encuentra o null en caso contrario.
     */
    private String obtenCampoInput(String campo, BufferedReader rd) {
        String line = "";
        String lineaTrimeada = "";
        String valor = "";

        try {
            int indValue = 0;
            while ((line = rd.readLine()) != null) {
                lineaTrimeada = line.trim();
                if (lineaTrimeada.contains("name=\"" + campo + "\"")) {
                    indValue = lineaTrimeada.indexOf("value");
                    if (indValue > 0) {
                        valor = lineaTrimeada.substring(indValue);
                        if (valor.startsWith("value")) {
                            indValue = valor.indexOf("\"");
                            valor = valor.substring(indValue + 1);
                            indValue = valor.indexOf("\"");
                            valor = valor.substring(0, indValue);
                            break;
                        }
                        else {
                            valor = "";
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            valor = "";
            e.printStackTrace();
        }
        log.info("######### CAMPO: " + campo + " | VALOR=" + valor);
        return valor;
    }

    /**
     * Se realiza un Get al url de pago a beneficiario para obtener el nuevo parametro __VIEWSTATE.
     * @param httpServiceDTO El DTO que contiene el HttpClient.
     * @return El HttpServiceDTO con el nuevo parametro __VIEWSTATE.
     */
    private HttpServiceDTO hacerGetUrlRegistroDatos(HttpServiceDTO httpServiceDTO) {
        String viewState = "";
        try {
            HttpGet httpgetregdatos = new HttpGet(this.getUrlAvanzaEtapa());

            log.info("######### haciendo request de obtener VIEWSTATE=" + this.getUrlAvanzaEtapa());
            HttpResponse responseregdatos = httpServiceDTO.getClienteHttp().execute(httpgetregdatos, httpServiceDTO.getContexto());

            log.info("######### status para viewState=" + responseregdatos.getStatusLine().getStatusCode());
            log.info("######### reason para viewState=" + responseregdatos.getStatusLine().getReasonPhrase());
            if (responseregdatos.getStatusLine().getStatusCode() != 200) {
                httpServiceDTO.setError(responseregdatos.getStatusLine().getReasonPhrase());
                httpServiceDTO.setViewState("");
                return httpServiceDTO;
            }

            HttpEntity responseEntity = responseregdatos.getEntity();
            if (responseEntity != null) {
                try {
                    responseEntity = new BufferedHttpEntity(responseEntity);
                    BufferedReader rd = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
                    viewState = this.obtenCampoInput("__VIEWSTATE", rd);
                    if (viewState == null) {
                        viewState = "";
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        } catch (ClientProtocolException cpe) {
            viewState = "";
            cpe.printStackTrace();
        } catch (IOException ioe) {
            viewState = "";
            ioe.printStackTrace();
        }

        httpServiceDTO.setViewState(viewState);
        return httpServiceDTO;
    }
    
    /**
     * Se realiza un Get al url de pago a beneficiario para obtener el nuevo parametro __VIEWSTATE.
     * @param httpServiceDTO El DTO que contiene el HttpClient.
     * @return El HttpServiceDTO con el nuevo parametro __VIEWSTATE.
     */
    private HttpServiceDTO hacerGetUrlBusquedaExpedienteGeneral(HttpServiceDTO httpServiceDTO) {
        String viewState = "";
        try {
            HttpGet httpgetregdatos = new HttpGet(this.getUrlBusqueda());

            log.info("######### haciendo request de obtener VIEWSTATE=" + this.getUrlAvanzaEtapa());
            HttpResponse responseregdatos = httpServiceDTO.getClienteHttp().execute(httpgetregdatos, httpServiceDTO.getContexto());

            log.info("######### status para viewState=" + responseregdatos.getStatusLine().getStatusCode());
            log.info("######### reason para viewState=" + responseregdatos.getStatusLine().getReasonPhrase());
            if (responseregdatos.getStatusLine().getStatusCode() != 200) {
                httpServiceDTO.setError(responseregdatos.getStatusLine().getReasonPhrase());
                httpServiceDTO.setViewState("");
                return httpServiceDTO;
            }

            HttpEntity responseEntity = responseregdatos.getEntity();
            if (responseEntity != null) {
                try {
                    responseEntity = new BufferedHttpEntity(responseEntity);
                    BufferedReader rd = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
                    viewState = this.obtenCampoInput("__VIEWSTATE", rd);
                    if (viewState == null) {
                        viewState = "";
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        } catch (ClientProtocolException cpe) {
            viewState = "";
            cpe.printStackTrace();
        } catch (IOException ioe) {
            viewState = "";
            ioe.printStackTrace();
        }

        httpServiceDTO.setViewState(viewState);
        return httpServiceDTO;
    }
    /**
     * Realiza la busqueda de un expediente en el portal de fovissste.
     * @param expediente El expediente a buscar.
     * @return El objeto HttpServiceDTO que contiene el HttpResponse con la respuesta del portal.
     */
    private HttpServiceDTO realizaBusqueda(Expediente expediente, HttpServiceDTO httpServiceDTO) {
        HttpResponse responsebusqueda = null;

        log.info("######### __VIEWSTATE antes peticion=" + httpServiceDTO.getViewState());
        // Se hace un Get al url de BUsqueda Expediente General y asi obtener el nuevo
        // valor del parametro __VIEWSTATE.
        httpServiceDTO = this.hacerGetUrlBusquedaExpedienteGeneral(httpServiceDTO);

        log.info("######### __VIEWSTATE=" + httpServiceDTO.getViewState());
        log.info("######### NumeroExpedienteTextBox=" + expediente.getNumeroSolicitud());

        List<NameValuePair> formbusqueda = new ArrayList<NameValuePair>();
        formbusqueda.add(new BasicNameValuePair("BuscarButton", "Buscar"));
        formbusqueda.add(new BasicNameValuePair("NumeroExpedienteTextBox", expediente.getNumeroSolicitud()));
        formbusqueda.add(new BasicNameValuePair("RFCTextBox", ""));
        formbusqueda.add(new BasicNameValuePair("CURPTextbox", ""));
        formbusqueda.add(new BasicNameValuePair("NombreTextBox", ""));
        formbusqueda.add(new BasicNameValuePair("ApellidoPaternoTextBox", ""));
        formbusqueda.add(new BasicNameValuePair("ApellidoMaternoTextBox", ""));
        formbusqueda.add(new BasicNameValuePair("__VIEWSTATE", httpServiceDTO.getViewState()));
        formbusqueda.add(new BasicNameValuePair("__VIEWSTATEENCRYPTED", ""));

        try {
            UrlEncodedFormEntity entitybusqueda = new UrlEncodedFormEntity(formbusqueda, "UTF-8");
            HttpPost httppostbusqueda = new HttpPost(this.getUrlBusqueda());
            httppostbusqueda.setEntity(entitybusqueda);
//            HttpClient httpclientbusqueda = new DefaultHttpClient();

            log.info("######### haciendo request de busqueda=" + this.getUrlBusqueda());
            /***************************************
             * Liberando conexiones con posibles errores 
             * ***************************************/
            // HttpClient clienteHttp = httpServiceDTO.getClienteHttp();
            // ClientConnectionManager connectionManager = clienteHttp.getConnectionManager();
            // connectionManager.shutdown();
            /*******************************************************/
            responsebusqueda = httpServiceDTO.getClienteHttp().execute(httppostbusqueda, httpServiceDTO.getContexto());
            httpServiceDTO.setRespHttp(responsebusqueda);

            log.info("######### status de busqueda=" + responsebusqueda.getStatusLine().getStatusCode());
            log.info("######### reason de busqueda=" + responsebusqueda.getStatusLine().getReasonPhrase());
            if (responsebusqueda.getStatusLine().getStatusCode() != 200) {
                httpServiceDTO.setError(responsebusqueda.getStatusLine().getReasonPhrase());
            }
        } catch (IllegalStateException ise) {
        	// Para el caso de recibir un ERROR 500 desde el servidor y poder liberar el recurso
        	ise.printStackTrace();
        	HttpClientUtils.closeQuietly(httpServiceDTO.getClienteHttp());
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
        } catch (ClientProtocolException cpe) {
            cpe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return httpServiceDTO;
    }

    /**
     * Revisa en la pantalla de Expediente verificación si las fechas indican existencia de recursos
     * @param httpServiceDTO El DTO con el objeto HttpClient de la autenticacion.
     * @return Un DTO con los datos del envio del pago a beneficiario.
     */
    private HttpServiceDTO obtenerDisponibilidadRecursos(HttpServiceDTO httpServiceDTO) {
    
       	    
            try {
            	// FASE 1 enviar peticion a Actualizar Session
                URIBuilder builder = new URIBuilder();
                builder.setScheme(this.getUrlAvanzaEtapaScheme())
                    .setHost(this.getUrlAvanzaEtapaHost())
                    .setPath(this.getUrlAvanzaEtapaPath())
                    .setParameter("ID_Expediente", httpServiceDTO.getIdExpediente())
                    .setParameter("ID_Fase", "6");
                URI uri = builder.build();
            	HttpGet httpgetregdatos = new HttpGet(uri);

                log.info("######### haciendo request para datos de Expediente =" + httpgetregdatos.getURI());

                HttpResponse responseregdatos = httpServiceDTO.getClienteHttp().execute(httpgetregdatos, httpServiceDTO.getContexto());

                log.info("######### status para viewState=" + responseregdatos.getStatusLine().getStatusCode());
                log.info("######### reason para viewState=" + responseregdatos.getStatusLine().getReasonPhrase());
                
                if (responseregdatos.getStatusLine().getStatusCode() != 200) {
                	httpServiceDTO.setError(responseregdatos.getStatusLine().getReasonPhrase());
                    httpServiceDTO.setViewState("");
                }
                else {
                   	obtenerFechasDesdeExpediente(responseregdatos, httpServiceDTO);
                }
                
            } catch(URISyntaxException URIse) {
            	URIse.printStackTrace();
            } catch (ClientProtocolException cpe) {
                cpe.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

            return httpServiceDTO;
    }


	/**
	 * @param fechas
	 * @param responseregdatos
	 * @throws IOException
	 */
	private void obtenerFechasDesdeExpediente(HttpResponse responseregdatos, HttpServiceDTO httpServiceDTO) throws IOException {
		
		Map<String, String> camposForma = new HashMap<String, String>();
		HttpEntity responseEntity = responseregdatos.getEntity();
		String fechaAutorizacion = "";
		String fechaVencimiento = "";
		String notariaMensaje = "";
		String viewState = "";
		
		
		if (responseEntity != null) {
		    responseEntity = new BufferedHttpEntity(responseEntity);
		    BufferedReader rdFechaAutorizacion = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
		    fechaAutorizacion = this.obtenFechaAutorizacionFirma(rdFechaAutorizacion);
		    if (fechaAutorizacion == null) {
		    	fechaAutorizacion = "";
		    }
		    BufferedReader rdFechaVencimiento = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
		    fechaVencimiento = this.obtenFechaVencimiento(rdFechaVencimiento);
		    if (fechaVencimiento == null) {
		    	fechaVencimiento = "";
		    }
		    BufferedReader rdNotaria = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
		    notariaMensaje = this.obtenNotariaMensaje(rdNotaria);
		    if (notariaMensaje == null) {
		    	notariaMensaje = "";
		    }
		    BufferedReader rdViewState = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
		    viewState = this.obtenCampoInput("__VIEWSTATE", rdViewState);
		    if (viewState == null) {
		    	viewState = "";
		    }

            httpServiceDTO.setFechaAutorizacion(fechaAutorizacion);
            httpServiceDTO.setFechaVencimiento(fechaVencimiento);
            httpServiceDTO.setEtapaNotaria(notariaMensaje);
            httpServiceDTO.setViewState(viewState);


		    for(String label: CAMPOS_FORMA_VERIFICACION){
		    	BufferedReader rdCampos = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
		    	String valor = this.obtenCampoInput(label, rdCampos);
		    	log.debug("######### Etiqueta =[" + label + "], Valor[" + valor + "]");
		    	if(valor == null) {
		    		valor = "";
		    	}
		    	camposForma.put(label, valor);
		    }

		    httpServiceDTO.setCamposForma(camposForma);
		}
	}

    /**
     * @param httpServiceDTO
     * @return
     */
    private HttpServiceDTO avanzarExpedienteNotaria (HttpServiceDTO httpServiceDTO) {
    	

    	HttpResponse responseAvanzar; 
        List<NameValuePair> formAvanzaNotaria = new ArrayList<NameValuePair>();
        formAvanzaNotaria.add(new BasicNameValuePair("__VIEWSTATE", httpServiceDTO.getViewState()));
        formAvanzaNotaria.add(new BasicNameValuePair("btnAceptar", ":: ACEPTAR ::"));
        Map<String,String> camposAdicionales = httpServiceDTO.getCamposForma();
        Iterator<Entry<String, String>> it = camposAdicionales.entrySet().iterator();
        
        while (it.hasNext()) {
            Map.Entry<String,String> pairs = it.next();
            log.debug("########## " +  pairs.getKey() + " = " + pairs.getValue());
            formAvanzaNotaria.add(new BasicNameValuePair(pairs.getKey(), pairs.getValue()));
            it.remove(); // avoids a ConcurrentModificationException
        }

        try {
            UrlEncodedFormEntity entitybusqueda = new UrlEncodedFormEntity(formAvanzaNotaria, "UTF-8");
            HttpPost httppostbusqueda = new HttpPost(this.getUrlAvanzaNotaria());
            httppostbusqueda.setEntity(entitybusqueda);

            log.info("######### haciendo request de aceptar Avance =" + this.getUrlAvanzaNotaria());
            responseAvanzar = httpServiceDTO.getClienteHttp().execute(httppostbusqueda, httpServiceDTO.getContexto());
            httpServiceDTO.setRespHttp(responseAvanzar);

            log.info("######### status de solicitud Avanzar=" + responseAvanzar.getStatusLine().getStatusCode());
            log.info("######### reason de solicitud Avanzar=" + responseAvanzar.getStatusLine().getReasonPhrase());
            if (responseAvanzar.getStatusLine().getStatusCode() != 200) {
                httpServiceDTO.setError(responseAvanzar.getStatusLine().getReasonPhrase());
            }
            
//            HttpEntity responseEntity = httpServiceDTO.getRespHttp().getEntity();
//            if (responseEntity != null) {
//                responseEntity = new BufferedHttpEntity(responseEntity);
//                BufferedReader rd = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
//                
//            }

        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
        } catch (ClientProtocolException cpe) {
            cpe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return httpServiceDTO;

    }
    
    
    @Override
    public DatosRegresoDTO logOut(HttpServiceDTO httpServiceDTO) {
    	DatosRegresoDTO dr = new DatosRegresoDTO();

    	try {
            HttpGet httpgetlogout = new HttpGet(this.getUrlLogout());

            log.info("######### haciendo request de logout=" + this.getUrlLogout());
            HttpResponse responselogout = httpServiceDTO.getClienteHttp().execute(httpgetlogout, httpServiceDTO.getContexto());

            log.info("######### logout status (Status Code)  =" + responselogout.getStatusLine().getStatusCode());
            log.info("######### logout status (Reason phrase)=" + responselogout.getStatusLine().getReasonPhrase());
            HttpEntity responseEntity = responselogout.getEntity();
            if (responseEntity != null) {
                responseEntity = new BufferedHttpEntity(responseEntity);
                long largoRespuesta = responseEntity.getContentLength();
                log.debug("######### logout response length= [" + largoRespuesta + ']');
            	dr.setValido(true);
            	dr.setError("");
            }
            else{
            	dr.setValido(false);
            	dr.setError("No se pudo realizar el logout correctamente");
            }
        } catch (ClientProtocolException cpe) {
        	dr.setValido(false);
        	dr.setError("No se pudo realizar el logout correctamente");
        	cpe.printStackTrace();
        } catch (IOException ioe) {
        	dr.setValido(false);
        	dr.setError("No se pudo realizar el logout correctamente");
        	ioe.printStackTrace();
        } catch (Exception e) {
        	dr.setValido(false);
        	dr.setError("No se pudo realizar el logout correctamente");
        	e.printStackTrace();
        } 
        return dr;
    }

    public String getUrlLogin() {
        return urlLogin;
    }

    public String getParamUsuario() {
        return paramUsuario;
    }

    public String getParamPassword() {
        return paramPassword;
    }

    public String getParamToken() {
        return paramToken;
    }

    public void setUrlLogin(String urlLogin) {
        this.urlLogin = urlLogin;
    }

    public void setParamUsuario(String paramUsuario) {
        this.paramUsuario = paramUsuario;
    }

    public void setParamPassword(String paramPassword) {
        this.paramPassword = paramPassword;
    }

    public void setParamToken(String paramToken) {
        this.paramToken = paramToken;
    }

    public String getUsaToken() {
        return usaToken;
    }

    public void setUsaToken(String usaToken) {
        this.usaToken = usaToken;
    }


    public String getUrlBusqueda() {
        return urlBusqueda;
    }

    public void setUrlBusqueda(String urlBusqueda) {
        this.urlBusqueda = urlBusqueda;
    }

    public String getUrlLogout() {
        return urlLogout;
    }

    public void setUrlLogout(String urlLogout) {
        this.urlLogout = urlLogout;
    }

    public String getUrlInicio() {
        return urlInicio;
    }

    public void setUrlInicio(String urlInicio) {
        this.urlInicio = urlInicio;
    }

    public String getUrlAvanzaEtapa() {
        return urlAvanzaEtapa;
    }

    public void setUrlAvanzaEtapa(String urlAvanzaEtapa) {
        this.urlAvanzaEtapa = urlAvanzaEtapa;
    }

	/**
	 * @return the urlAvanzaNotaria
	 */
	public String getUrlAvanzaNotaria() {
		return urlAvanzaNotaria;
	}

	/**
	 * @param urlAvanzaNotaria the urlAvanzaNotaria to set
	 */
	public void setUrlAvanzaNotaria(String urlAvanzaNotaria) {
		this.urlAvanzaNotaria = urlAvanzaNotaria;
	}


	/**
	 * @return the urlVerificacionFinal
	 */
	public String getUrlVerificacionFinal() {
		return urlVerificacionFinal;
	}


	/**
	 * @param urlVerificacionFinal the urlVerificacionFinal to set
	 */
	public void setUrlVerificacionFinal(String urlVerificacionFinal) {
		this.urlVerificacionFinal = urlVerificacionFinal;
	}


	/**
	 * @return the urlAvanzaEtapaHost
	 */
	public String getUrlAvanzaEtapaHost() {
		return urlAvanzaEtapaHost;
	}


	/**
	 * @param urlAvanzaEtapaHost the urlAvanzaEtapaHost to set
	 */
	public void setUrlAvanzaEtapaHost(String urlAvanzaEtapaHost) {
		this.urlAvanzaEtapaHost = urlAvanzaEtapaHost;
	}


	/**
	 * @return the urlAvanzaEtapaScheme
	 */
	public String getUrlAvanzaEtapaScheme() {
		return urlAvanzaEtapaScheme;
	}


	/**
	 * @param urlAvanzaEtapaScheme the urlAvanzaEtapaScheme to set
	 */
	public void setUrlAvanzaEtapaScheme(String urlAvanzaEtapaScheme) {
		this.urlAvanzaEtapaScheme = urlAvanzaEtapaScheme;
	}


	/**
	 * @return the urlAvanzaEtapaPath
	 */
	public String getUrlAvanzaEtapaPath() {
		return urlAvanzaEtapaPath;
	}


	/**
	 * @param urlAvanzaEtapaPath the urlAvanzaEtapaPath to set
	 */
	public void setUrlAvanzaEtapaPath(String urlAvanzaEtapaPath) {
		this.urlAvanzaEtapaPath = urlAvanzaEtapaPath;
	}

}
