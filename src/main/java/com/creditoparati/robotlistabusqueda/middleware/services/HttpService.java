package com.creditoparati.robotlistabusqueda.middleware.services;

import com.creditoparati.robotlistabusqueda.middleware.services.util.DatosRegresoDTO;
import com.creditoparati.robotlistabusqueda.middleware.services.util.HttpServiceDTO;
import com.creditoparati.robotlistabusqueda.model.Expediente;

/**
 * Interface del servicio que proporciona servicios de http.
 * @author Edgar Deloera
 */
public interface HttpService {


    /**
     * Realiza el envio de un expediente via http a Fovissste.
     * @param credito El expediente a enviar.
     * @param httpServiceDTO El DTO con el expediente http usado en el proceso de autenticacion.
     * @return Un DTO con los datos resultado del envio del expediente.
     * @throws Exception
     */
    DatosRegresoDTO enviaExpediente(Expediente expediente, HttpServiceDTO httpServiceDTO) throws Exception;

    /**
     * Realiza la revision de los datos del expediente con una consulta al portal de Fovissste.
     * @param expediente El expediente a revisar.
     * @param httpServiceDTO El DTO con el cliente http usado en el proceso de autenticacion.
     * @return Un DTO con los datos resultado del envio del credito.
     * @throws Exception
     */
    DatosRegresoDTO revisionExpediente(Expediente expediente, HttpServiceDTO httpServiceDTO) throws Exception;

    /**
     * Realiza la autentificacion al sitio de Fovissste.
     * @param httpServiceDTO El DTO con el cliente http usado en el proceso de autenticacion.
     * @return Un DTO el rsultado de la operacion.
     * @throws Exception En caso de error.
     */
    DatosRegresoDTO autentificarAntesProcesar(HttpServiceDTO httpServiceDTO) throws Exception;

    /**
     * Realiza el log out del portal de fovissste.
     * @param httpServiceDTO El DTO con el cliente http usado en el proceso de autenticacion.
     * @throws Exception
     */
    DatosRegresoDTO logOut(HttpServiceDTO httpServiceDTO);

}
