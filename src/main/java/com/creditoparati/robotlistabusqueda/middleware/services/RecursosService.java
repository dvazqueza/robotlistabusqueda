package com.creditoparati.robotlistabusqueda.middleware.services;

import java.util.List;

import com.creditoparati.robotlistabusqueda.model.Expediente;

/**
 * Interface del servicio que realiza servicios de creditos.
 * @author Edgar Deloera
 */
public interface RecursosService {

    /**
     * Obtiene el listado de Creditos para enviar a Fovissste.
     * @return El listado de pagos por realizar.
     * @throws Exception En caso de error.
     */
	List<Expediente> getExpedientes() throws Exception;

	/**
	 * Obtiene el numero total de creditos en la BD para ser procesados.
	 * @return El numero de creditos en la BD.
	 * @throws Exception
	 */
	int getTotalExpedientes() throws Exception;

}
