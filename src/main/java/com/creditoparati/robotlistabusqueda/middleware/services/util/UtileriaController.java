package com.creditoparati.robotlistabusqueda.middleware.services.util;

import java.util.Calendar;
import java.util.List;

import com.creditoparati.robotlistabusqueda.model.Bitacora;

public class UtileriaController {

    /**
     * Arma el JSON de respuesta del lanzamiento del Robot.
     * @param respuesta El string de exito o falla.
     * @param creditos El numero de creditos a procesar.
     * @param error Cadena de error si existe.
     * @return La cadena en JSON de respuesta del lanzamiento.
     */
    public static String armaJSONRespuestaLanzarRobot(String respuesta, int creditos, String error) {
        String tiempo = "";
        Calendar cal = Calendar.getInstance();
        // mon dd yyyy hh:miAM (or PM), ejemplos: dec 18 2012 10:43:05:50AM / dec 18 2012 16:44:28:30PM
        tiempo = UtileriaController.getMes(cal.get(Calendar.MONTH) + 1) + " " +
                 cal.get(Calendar.DAY_OF_MONTH) + " " + 
                 cal.get(Calendar.YEAR) + " " +
                 cal.get(Calendar.HOUR_OF_DAY) + ":" + 
                 cal.get(Calendar.MINUTE) + ":" + 
                 cal.get(Calendar.SECOND) + ":" + 
                 cal.get(Calendar.MILLISECOND); 
        // + UtileriaController.getAM_PM(cal.get(Calendar.HOUR_OF_DAY));

        return "{" + 
                 "\"robot\": " + "\"" + respuesta + "\", " + 
                 "\"creditos\": " + "\"" + creditos + "\", " +
                 "\"caderror\": " + "\"" + error + "\", " +
                 "\"tiempo\": " + "\"" + tiempo + "\"" + 
               "}";
    }

    /**
     * Obtiene el mes en forma de cadena abreviada o corta.
     * @param mesInt El mes numerico.
     * @return La cadena del mes.
     */
    public static String getMes(int mesInt) {
        String mes = "";
        switch (mesInt) {
        case 1:
            mes = "jan";
            break;
        case 2:
            mes = "feb";
            break;
        case 3:
            mes = "mar";
            break;
        case 4:
            mes = "apr";
            break;
        case 5:
            mes = "may";
            break;
        case 6:
            mes = "jun";
            break;
        case 7:
            mes = "jul";
            break;
        case 8:
            mes = "aug";
            break;
        case 9:
            mes = "sep";
            break;
        case 10:
            mes = "oct";
            break;
        case 11:
            mes = "nov";
            break;
        case 12:
            mes = "dec";
            break;
        default:
            break;
        }
        return mes;
    }

    /**
     * Arma el JSON de respuesta del polling del procesamiento del robot.
     * @param respuesta El string de exito o falla.
     * @param procesados El numero de creditos procesados.
     * @param error Cadena de error si existe.
     * @return La cadena en JSON de respuesta del polling.
     */
    public static String armaJSONRespuestaPolling(String respuesta, int procesados, String error) {
        return "{" + 
                 "\"polling\": " + "\"" + respuesta + "\", " + 
                 "\"procesados\": " + "\"" + procesados + "\", " +
                 "\"caderror\": " + "\"" + error + "\"" + 
               "}";
    }

    /**
     * Arma el JSON de respuesta de la peticion del reporte de errores.
     * @param bitacoras Las bitacoras erroneas.
     * @return La cadena en JSON de respuesta con los errores.
     */
    public static String armaJSONRespuestaReporte(List<Bitacora> bitacoras) {
        String respuesta = "";
        if (bitacoras != null && bitacoras.size() > 0) {
            Bitacora bit = null;
            if (bitacoras.size() == 1) {
                bit = bitacoras.get(0);
                respuesta = 
                  "{" + 
                    "\"numsolicitud\": " + "\"" + bit.getNumeroSolicitud() + "\", " + 
                    "\"desarrolladora\": " + "\"" + bit.getIdDesarrolladora() + "\", " +
                    "\"fechafirma\": " + "\"" + bit.getFechaFirma().toString() + "\", " +
                    "\"estado\": " + "\"" + "fallido"  + "\", " +
                    "\"nombre\": " + "\"" + UtileriaController.escapaCaracteres(bit.getNombre()) + "\"" + 
                  "}";
            }
            else {
                String objsBitacoras = "";
                for (int i = 0; i < bitacoras.size(); i++) {
                    bit = bitacoras.get(i);
                    objsBitacoras += 
                      "{" + 
                        "\"numsolicitud\": " + "\"" + bit.getNumeroSolicitud() + "\", " + 
                        "\"desarrolladora\": " + "\"" + bit.getIdDesarrolladora() + "\", " +
                        "\"fechafirma\": " + "\"" + bit.getFechaFirma().toString() + "\", " +
                        "\"estado\": " + "\"" + "fallido" + "\", " +
                        "\"nombre\": " + "\"" + UtileriaController.escapaCaracteres(bit.getNombre()) + "\"" + 
                      "}";
                    if ( !((i + 1) == bitacoras.size()) ) {
                        objsBitacoras += ",";
                    }
                }
                respuesta = "[" +
                                objsBitacoras + 
                            "]";
            }
        }
        else {
            respuesta = 
                "{" + 
                  "\"noerrores\": " + "\"" + "1" + "\"" + 
                "}";
        }
        return respuesta;
    }

    /**
     * Escape de caracteres.
     * @param cadena La cadena a la que hay que escaparle caracteres.
     * @return La cadena con los caracteres escapados.
     */
    private static String escapaCaracteres(String cadena) {
        String escapada = "";
        if (cadena != null) {
            escapada = cadena.replace("\\", "\\\\");
        }
        return escapada;
    }

    /**
     * Retorna la etapa en forma de descripcion de acuerdo al numero de etapa recibido.
     * @param etapa El numero de etapa a obtener la descripcion.
     * @return La descripcion de la etapa correspondiente.
     */
    private static String getDescripcionEtapa(long etapa) {
        int intEtapa = (new Long(etapa)).intValue();
        String descEtapa = "?";
        switch (intEtapa) {
        case 0: 
            descEtapa = "info. completa";
            break;
        case 1: 
            descEtapa = "Validación";
            break;
        case 2: 
            descEtapa = "Envío";
            break;
        case 3: 
            descEtapa = "Reintento";
            break;
        case 4: 
            descEtapa = "Envío CP";
            break;
        case 5: 
            descEtapa = "Revisión";
            break;
        case 6: 
            descEtapa = "Revisión CP";
            break;
        default:
            break;
        }
        return descEtapa;
    }

}
