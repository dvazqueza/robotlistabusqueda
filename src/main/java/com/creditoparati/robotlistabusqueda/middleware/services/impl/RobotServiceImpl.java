package com.creditoparati.robotlistabusqueda.middleware.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.KeyMatcher;

import com.creditoparati.robotlistabusqueda.middleware.job.RobotListaBusquedaJob;
import com.creditoparati.robotlistabusqueda.middleware.job.RobotListaBusquedaListener;
import com.creditoparati.robotlistabusqueda.middleware.services.BitacoraService;
import com.creditoparati.robotlistabusqueda.middleware.services.RobotService;
import com.creditoparati.robotlistabusqueda.middleware.services.ServiceController;
import com.creditoparati.robotlistabusqueda.middleware.services.util.DatosRegresoDTO;
import com.creditoparati.robotlistabusqueda.middleware.services.util.HttpServiceDTO;
import com.creditoparati.robotlistabusqueda.model.Bitacora;

/**
 * Clase de servicio para el robot.
 * @author Edgar Deloera
 * @author Daniel Vazquez
 */
public class RobotServiceImpl implements RobotService {

    /**
     * Logging.
     */
    private static final Logger log = Logger.getLogger(RobotServiceImpl.class);

    /**
     * Milisegundos de retardo para comenzar el job.
     */
    private long retardoParaComienzo = 2000;

    /**
     * Servicio de creditos.
     */
    private BitacoraService bitacoraService;

    /**
     * @see com.creditoparati.robotpagocredito.middleware.services.RobotService#inicializaRobot()
     */
    public DatosRegresoDTO inicializaRobot(HttpServiceDTO httpServiceDTO) {
        log.info("######### Inicializando Robot...");
        DatosRegresoDTO dr = new DatosRegresoDTO();

        JobKey jobKey = new JobKey("robotListaBusquedaJob", "buscarecursos");
        JobDetail job = JobBuilder.newJob(RobotListaBusquedaJob.class)
                        .withIdentity(jobKey).build();

        long longFechaComienzo = (new Date().getTime()) + retardoParaComienzo;
        Date fechaComienzo = new Date(longFechaComienzo);

        SimpleTrigger trigger = (SimpleTrigger) TriggerBuilder.newTrigger()
                                .withIdentity("robotListaBusquedaTrigger", "buscarecursos")
                                .startAt(fechaComienzo)
                                .forJob("robotListaBusquedaJob", "buscarecursos") // identify job with name, group strings
                                .build();

        job.getJobDataMap().put(RobotListaBusquedaJob.HTTPSERVICEDTO, httpServiceDTO);

        Scheduler scheduler;
        RobotListaBusquedaListener listener = new RobotListaBusquedaListener();
        try {
            scheduler = (new StdSchedulerFactory()).getScheduler();
            scheduler.getListenerManager().addJobListener(listener, KeyMatcher.keyEquals(jobKey));
            scheduler.start();
            scheduler.scheduleJob(job, trigger);
            Thread.sleep(60L * 1000L);
            scheduler.shutdown(true);
            dr.setValido(true);
        } catch (SchedulerException e) {
        	log.error("######### Se detecto un SchedulerException ...");
            e.printStackTrace();
            dr.setValido(false);
            dr.setError(e.getMessage());
        } catch (InterruptedException e) {
        	log.error("######### Se detecto un InterruptedException ...");
			e.printStackTrace();
		}
        
    	log.info("######### Saliendo de inicializaRobot ...");
        return dr;
    }

    /**
     * @see com.creditoparati.robotpagocredito.middleware.services.RobotService#pollingProcesoDePago(String)
     */
    public DatosRegresoDTO pollingBusquedaRecursos(String tiempo) {
        log.info("######### Polling proceso...");
        DatosRegresoDTO dr = new DatosRegresoDTO();

        int procesados = this.getBitacoraService().obtenExpedientesProcesados(tiempo);

        dr.setValido(true);
        dr.getDatos().add("" + procesados);

        return dr;
    }


    @Override
    public List<Bitacora> getReporteListaBusqueda(String tiempo) {
        log.info("######### Obteniendo reporte de pagos erroneos...");
        List<Bitacora> pagosErroneos = new ArrayList<Bitacora>();

        try {
            pagosErroneos = this.getBitacoraService().obtenExpedientesErroneos(tiempo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pagosErroneos;
    }

    public BitacoraService getBitacoraService() {
        // Obteniendo bean del Servicio de Bitacoreo.
        if (this.bitacoraService == null) {
            this.bitacoraService = (BitacoraService) ServiceController.getService("bitacoraService");
        }
        return bitacoraService;
    }

    public void setBitacoraService(BitacoraService bitacoraService) {
        this.bitacoraService = bitacoraService;
    }

}
