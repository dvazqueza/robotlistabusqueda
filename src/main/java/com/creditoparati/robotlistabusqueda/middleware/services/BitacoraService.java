package com.creditoparati.robotlistabusqueda.middleware.services;

import java.util.List;

import com.creditoparati.robotlistabusqueda.model.Bitacora;
import com.creditoparati.robotlistabusqueda.model.Expediente;

/**
 * Interface del servicio que proporciona servicios de bitacoreo.
 * @author Edgar Deloera
 * @author Daniel Vazquez
 */
public interface BitacoraService {

    /**
     * Realiza el bitacoreo de un expediente.
     * @param expediente El expediente a bitacorar.
     * @param etapa La etapa a bitacorar.
     * @param error El string de error si es que lo hubo.
     * @return El numero de registros insertados.
     * @throws Exception
     */
    int realizaBitacoreo(Expediente expediente, String etapa, String error) throws Exception;

    /**
     * Realiza la obtencion del numero de expedientes procesados posterior a cierto tiempo.
     * @param tiempo El tiempo que servira de base para realizar la seleccion de procesos.
     * @return El numero de expedientes modificados.
     */
    int obtenExpedientesProcesados(String tiempo);

    /**
     * Realiza la obtencion de pagos de expedientes erroneos de la bitacora.
     * @param tiempo El tiempo que servira de base para realizar la seleccion de procesos.
     * @return El listado de bitacoras.
     * @throws Exception
     */
    List<Bitacora> obtenExpedientesErroneos(String tiempo) throws Exception;

}
