package com.creditoparati.robotlistabusqueda.middleware.services.impl;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.creditoparati.robotlistabusqueda.middleware.services.BitacoraService;
import com.creditoparati.robotlistabusqueda.middleware.services.error.ProcesoListaBusquedaException;
import com.creditoparati.robotlistabusqueda.middleware.services.RecursosService;
import com.creditoparati.robotlistabusqueda.middleware.services.HttpService;
import com.creditoparati.robotlistabusqueda.middleware.services.ListaBusquedaService;
import com.creditoparati.robotlistabusqueda.middleware.services.ServiceController;
import com.creditoparati.robotlistabusqueda.middleware.services.ValidadorInformacionService;
import com.creditoparati.robotlistabusqueda.middleware.services.util.DatosRegresoDTO;
import com.creditoparati.robotlistabusqueda.middleware.services.util.HttpServiceDTO;
import com.creditoparati.robotlistabusqueda.model.Expediente;

public class ListaBusquedaServiceImpl implements ListaBusquedaService {

    /**
     * Log de la clase
     */
    private static final Logger log = Logger.getLogger(ListaBusquedaServiceImpl.class);

	/**
	 * Servicio de busqueda de recursos.
	 */
	private RecursosService recursosService;

    /**
     * Servicio de validacion de informacion.
     */
    private ValidadorInformacionService validadorInformacionService;

    /**
     * Servicio de expedientes.
     */
    private BitacoraService bitacoraService;

    /**
     * Servicio de HTTP.
     */
    private HttpService httpService;

    /**
     * Propiedad inyectada que indica si se tiene que realizar la etapa cero que es:
     * la VALIDACION DE INFORMACION COMPLETA. 
     */
    private String etapaCero;

    /**
     * Propiedad inyectada que indica cuantos reintentos se deben hacer del envio. 
     */
    private String reintentos;

    /**
     * @see com.expedienteparati.robotpagoexpediente.middleware.services.PagaExpedientesService#procesoPagoExpedientes(HttpServiceDTO)
     */
    public void procesoListaBusqueda(HttpServiceDTO httpServiceDTO) throws Exception {
        log.debug("######### Procesando avance de expedientes en etapa 6 con fecha asignada...");

        // Se obtienen los expedientes a enviar a Fovissste.
        List<Expediente> expedientes = this.obtenerExpedientes();
        
        if (!expedientes.isEmpty()) {
			// SE REALIZA LA AUTENTICACION AL PORTAL DE FOVISSSTE.
			DatosRegresoDTO dr = this.obtenHttpService().autentificarAntesProcesar(httpServiceDTO);
			if (dr.isValido()) {
				for (Expediente expediente : expedientes) {
					log.info("######### numSolicitud ["+ expediente.getNumeroSolicitud() + ']');
					// boolean envioExitoso = false;

					// envioExitoso = this.enviaExpediente(expediente,httpServiceDTO);
		            dr = this.obtenHttpService().enviaExpediente(expediente, httpServiceDTO);
		            log.info("######### Envio valido=" + dr.isValido());
		            if (dr.isValido()) {
		                this.bitacoreaEnvioExpedienteValido(expediente, "6");
		            }
		            else {
		                this.bitacoreaEnvioExpedienteNoValido(expediente, "6", dr.getError());
				        int aReintentar = 1;

				        // Se establece la cantidad de reintentos por si falla la primera vez del envio HTTP.
				        if (this.getReintentos() != null && this.getReintentos().length() > 0) {
				            aReintentar = Integer.parseInt(this.getReintentos());
				        }
				    	
			        	for (int i = 1; i <= aReintentar; i++) {
			        		log.info("######### Haciendo reintento [" + i +"] de ["+ aReintentar +"]");
			        		dr = this.obtenHttpService().enviaExpediente(expediente, httpServiceDTO);
			                if (dr.isValido()) {
			                    this.bitacoreaEnvioExpedienteValido(expediente, "6");
			                    break;
			                }
			                else {
			                	this.bitacoreaEnvioExpedienteNoValido(expediente, "6", dr.getError());
			                }
			            }
		            }
				}

				// EL ULTIMO PUNTO ES HACER LOG OUT DEL PORTAL DE FOVISSSTE.
				this.realizaLogOut(httpServiceDTO);

				log.debug("######### Total de expedientes:" + expedientes.size());
				log.debug("######### Terminando de procesar Lista Busqueda de expedientes ...");
			} else {
				log.debug("######### Total de expedientes:" + expedientes.size());
				log.debug("######### Error al intentar realizar el login a la aplicación ...");
				httpServiceDTO.setError(dr.getError());
				throw new ProcesoListaBusquedaException ("Al tratar de autentificarse al portal de FOVISSSTE. " + dr.getError());
			}
		}
        else {
        	httpServiceDTO.setError("No se encontraron Expedientes a Procesar");
        	throw new ProcesoListaBusquedaException("No se encontraron Expedientes a Procesar");
        }
    }

    /**
     * Realiza el log out del portal de fovissste.
     * @param httpServiceDTO El DTO con objetos necesarios para el servicio http.
     */
    private void realizaLogOut(HttpServiceDTO httpServiceDTO) {
        try {
        	log.debug("######### Invocando Logout");
            this.obtenHttpService().logOut(httpServiceDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Realiza el bitacoreo de que la etapa de envio de expediente tuvo error y 
     * no pasa a la siguiente etapa.
     * @param expediente El expediente validado.
     * @param etapa La etapa a bitacorar.
     * @param errores Un string con errores a bitacorar.
     */
    private void bitacoreaEnvioExpedienteNoValido(Expediente expediente, String etapa, String errores) {
        try {
            this.getBitacoraService().realizaBitacoreo(expediente, etapa, errores);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Realiza el bitacoreo de que la etapa de envio de expediente esta ok y 
     * pasa a la siguiente etapa.
     * @param expediente El expediente validado.
     * @param etapa La etapa a bitacorar.
     */
    private void bitacoreaEnvioExpedienteValido(Expediente expediente, String etapa) {
        try {
            this.getBitacoraService().realizaBitacoreo(expediente, etapa, "");
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Obtiene el servicio HttpService, si es nulo se obtiene del contexto de 
     * spring de lo contrario se regresa el servicio.
     * @return El servicio.
     * @throws Exception
     */
    private HttpService obtenHttpService() throws Exception {
        if (this.getHttpService() == null) {
            this.setHttpService((HttpService) ServiceController.getService("httpService"));
        }
        return this.getHttpService();
    }

    /**
     * Se obtiene el listado de expedientes para ser procesados y enviados a Fovissste.
     * @return El listado de expedientes.
     */
    private List<Expediente> obtenerExpedientes() {
        List<Expediente> lista = new Vector<Expediente>();

        // Obteniendo el servicio de Expedientes para obtener el listado.
        if (this.getRecursosService() == null) {
            this.setRecursosService((RecursosService) ServiceController.getService("recursosService"));
        }

        try {
            lista = this.getRecursosService().getExpedientes();
            if (lista == null) {
                lista = new Vector<Expediente>();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public RecursosService getRecursosService() {
        return recursosService;
    }

    public void setRecursosService(RecursosService recursosService) {
        this.recursosService = recursosService;
    }

    public ValidadorInformacionService getValidadorInformacionService() {
        return validadorInformacionService;
    }

    public void setValidadorInformacionService(
            ValidadorInformacionService validadorInformacionService) {
        this.validadorInformacionService = validadorInformacionService;
    }

    public String getEtapaCero() {
        return etapaCero;
    }

    public void setEtapaCero(String etapaCero) {
        this.etapaCero = etapaCero;
    }

    public HttpService getHttpService() {
        return httpService;
    }

    public void setHttpService(HttpService httpService) {
        this.httpService = httpService;
    }

    public String getReintentos() {
        return reintentos;
    }

    public void setReintentos(String reintentos) {
        this.reintentos = reintentos;
    }

    public BitacoraService getBitacoraService() {
        // Obteniendo bean del Servicio de Bitacoreo.
        if (this.bitacoraService == null) {
            this.bitacoraService = (BitacoraService) ServiceController.getService("bitacoraService");
        }
        return this.bitacoraService;
    }

    public void setBitacoraService(BitacoraService bitacoraService) {
        this.bitacoraService = bitacoraService;
    }

}
