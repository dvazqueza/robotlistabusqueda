package com.creditoparati.robotlistabusqueda.middleware.services;

import com.creditoparati.robotlistabusqueda.middleware.services.util.DatosRegresoDTO;
import com.creditoparati.robotlistabusqueda.model.Expediente;


/**
 * Interface del servicio que realiza la validacion de informacion de un credito.
 * @author Edgar Deloera
 */
public interface ValidadorInformacionService {

    /**
     * Realiza la validacion de la informacion del credito, como banco con clabe e email. 
     * A su vez inserta en la bitacora el resultado de la validacion.
     * @param credito El credito a validar.
     * @return Un DTO con el resultado de la validacion.
     * @throws Exception En caso de error.
     */
    DatosRegresoDTO informacionExpedienteValida(Expediente expediente) throws Exception;

	/**
	 * Realiza la validacion de que el credito tenga la informacion completa para ser procesado,
	 * a su vez inserta en la bitacora el resultado de la validacion.
	 * @param credito El credito a validar.
     * @return Un DTO con el resultado de la validacion.
	 * @throws Exception En caso de error.
	 */
	DatosRegresoDTO informacionExpedienteCompleta(Expediente expediente) throws Exception;

}
