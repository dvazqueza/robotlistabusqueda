package com.creditoparati.robotlistabusqueda.middleware.services.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.creditoparati.robotlistabusqueda.middleware.services.BitacoraService;
import com.creditoparati.robotlistabusqueda.middleware.services.ServiceController;
import com.creditoparati.robotlistabusqueda.model.Expediente;
import com.creditoparati.robotlistabusqueda.model.Bitacora;
import com.creditoparati.robotlistabusqueda.model.dao.BitacoraDAO;
import com.creditoparati.robotlistabusqueda.model.BusquedaRecursos;
import com.creditoparati.robotlistabusqueda.model.dao.BusquedaRecursosDAO;

public class BitacoraServiceImpl implements BitacoraService {

    /**
     * Log de la clase
     */
    private static final Logger log = Logger.getLogger(BitacoraServiceImpl.class);

    /**
     * DAO de Bitacora.
     */
    private BitacoraDAO bitacoraDao;

    /**
     * DAO de Bitacora.
     */
    private BusquedaRecursosDAO busquedaRecursosDao;

    @Override
    public int realizaBitacoreo(Expediente expediente, String etapa, String error) throws Exception {
        log.debug("######### Realizando Bitacoreo: " + expediente.getNumeroSolicitud() + " | " + etapa + " | " + error);
        BusquedaRecursos bitacora = null;

        bitacora = this.armaBusquedaRecurso(expediente, etapa, error);
        if (bitacora != null) {
            this.getBusquedaRecursosDao().save(bitacora);
        }
        return 1;
    }

    /**
     * Arma el objeto Bitacora que se insertara en la BD.
     * @param credito El credito de donde se tomaran los datos.
     * @param etapa La etapa que se esta bitacorando.
     * @param error El string con el error a insertar en la bitacora, si existe.
     * @return El objeto Bitacora armado.
     */
    private Bitacora armaBitacora(Expediente expediente, String etapa, String error) {
        Bitacora bitacora = new Bitacora();
        bitacora.setNumeroSolicitud(expediente.getNumeroSolicitud());
        bitacora.setFechaFirma(new Date());
        if (error != null && error != "") {
            bitacora.setEstatusBitacora(new Boolean(false));
            bitacora.setNombre(error);
        }
        else {
            bitacora.setEstatusBitacora(new Boolean(true));
            bitacora.setNombre("");
        }
        return bitacora;
    }

    /**
     * Arma el objeto Bitacora que se insertara en la BD.
     * @param credito El credito de donde se tomaran los datos.
     * @param etapa La etapa que se esta bitacorando.
     * @param error El string con el error a insertar en la bitacora, si existe.
     * @return El objeto Bitacora armado.
     */
    private BusquedaRecursos armaBusquedaRecurso(Expediente expediente, String etapa, String error) {
    	BusquedaRecursos busquedaRecurso = new BusquedaRecursos();
    	busquedaRecurso.setNumeroSolicitud(expediente.getNumeroSolicitud());
    	busquedaRecurso.setFechaFirma(new Date());
    	busquedaRecurso.setNombre(expediente.getNombre());
    	busquedaRecurso.setNumeroCliente(Long.parseLong(expediente.getNumeroCliente()));
        if (error != null && error != "") {
        	busquedaRecurso.setIdFase(Integer.parseInt(etapa));
        	busquedaRecurso.setComentario(error);
        }
        else {
        	busquedaRecurso.setIdFase(Integer.parseInt(etapa));
        	busquedaRecurso.setComentario("");
        }
        return busquedaRecurso;
    }
    
    @Override
    public int obtenExpedientesProcesados(String tiempo) {
        log.debug("######### Obteniendo pagos procesados...");
        int tot = 0;
        try {
            tot = this.getBitacoraDao().getTotalExpedientesProcesadosPorTiempo(tiempo);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return tot;
    }

    @Override
    public List<Bitacora> obtenExpedientesErroneos(String tiempo) throws Exception {
        return this.getBitacoraDao().findBitacorasErroneasPorTiempo(tiempo);
    }

    public BitacoraDAO getBitacoraDao() {
        if (bitacoraDao == null) {
            this.setBitacoraDao((BitacoraDAO) ServiceController.getService("bitacoraDAO"));
        }
        return bitacoraDao;
    }

    public void setBitacoraDao(BitacoraDAO bitacoraDao) {
        this.bitacoraDao = bitacoraDao;
    }

    public BusquedaRecursosDAO getBusquedaRecursosDao() {
        if (busquedaRecursosDao == null) {
            this.setBusquedaRecursosDao((BusquedaRecursosDAO) ServiceController.getService("busquedaRecursosDAO"));
        }
        return busquedaRecursosDao;
    }

    public void setBusquedaRecursosDao(BusquedaRecursosDAO busquedaRecursosDao) {
        this.busquedaRecursosDao = busquedaRecursosDao;
    }

}
