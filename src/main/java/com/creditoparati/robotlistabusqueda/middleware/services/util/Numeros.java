package com.creditoparati.robotlistabusqueda.middleware.services.util;

public enum Numeros {

    /**
    * 0 (cero)
    */
    _0(0),

    /**
    * 1
    */
    _1(1),

    /**
     * 2
     */
    _2(2),

    /**
     * 3
     */
    _3(3),

    /**
     * 4
     */
    _4(4),

    /**
     * 5
     */
    _5(5),

    /**
    * 6
    */
    _6(6),

    /**
     * 7
     */
    _7(7),

    /**
     * 8
     */
    _8(8),

    /**
     * 9
     */
    _9(9),

    /**
    * 10
    */
    _10(10),
    
    /**
     * 29
     */
    _28(28),
    /**
     * 30
     */
    _30(30),
    /**
    * 11
    */
    _11(11),

    /**
    * 14
    */
    _14(14),

    /**
    * 15
    */
    _15(15),

    /**
    * 23
    */
    _23(23),

    /**
    * 59
    */
    _59(59),
    
    /**
     * 100
     */
     _100(100),

    /**
    * 101
    */
    _101(101),

    /**
    * 102
    */
    _102(102),
    /**
     * 103
     */
    _103(103),
    /**
     * 104
     */
    _104(104),
    
    /**
     * 105
     */
    _105(105),

    /**
     * 501
     */
    _501(501),

    /**
     * 502
     */
    _502(502),
    /**
     * 5001
     */
    _5001(5001),

    /**
     * 5002
     */
    _5002(5002),
    
    /**
     * 5003
     */
    _5003(5003),
    /**
     * 5004
     */
    _5004(5004);
    
    /**
     * Su valor
     */
    private Integer valor;


    /**
     * Constructor
     * @param valor el valor
     */
    Numeros(Integer valor) {
        this.valor = valor;
    }
    
    /**
     * Convierte a int.
     * @return int
     */
    public int intValue() {
        return valor.intValue();
    }
    
    /**
     * Convierte a long.
     * @return long.
     */
    public long longValue() {
        return valor.longValue();
    }
    
    /**
     * Su valor en cadena.
     * @return Cadena
     */
    @Override
    public String toString() {
        return String.valueOf(valor);
    }
}
