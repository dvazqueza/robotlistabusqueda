package com.creditoparati.robotlistabusqueda.middleware.services.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.creditoparati.robotlistabusqueda.middleware.services.ExpedienteService;
import com.creditoparati.robotlistabusqueda.middleware.services.ServiceController;
import com.creditoparati.robotlistabusqueda.model.Expediente;
import com.creditoparati.robotlistabusqueda.model.dao.ExpedienteDAO;

public class ExpedienteServiceImpl implements ExpedienteService {

    /**
     * Log de la clase
     */
    private static final Logger log = Logger.getLogger(ExpedienteServiceImpl.class);

    /**
     * DAO de Expedientes.
     */
    private ExpedienteDAO expedientesDao;

    @Override
    public List<Expediente> getExpedientes() throws Exception {
        log.debug("######### Obteniendo listado de expedientes...");
        List<Expediente> expedientes = this.obtenExpedientesDao().loadAll();
        return expedientes;
    }

    @Override
    public int getTotalExpedientes() throws Exception {
        log.debug("######### Obteniendo el total de expedientes para lanzar el robot ...");
        return this.obtenExpedientesDao().getTotalExpedientes();
    }

    private ExpedienteDAO obtenExpedientesDao() {
        if (this.getExpedientesDao() == null) {
            this.setExpedientesDao((ExpedienteDAO) ServiceController.getService("expedienteDAO"));
        }
        return this.getExpedientesDao();
    }

    public ExpedienteDAO getExpedientesDao() {
        return expedientesDao;
    }

    public void setExpedientesDao(ExpedienteDAO expedientesDao) {
        this.expedientesDao = expedientesDao;
    }

}
