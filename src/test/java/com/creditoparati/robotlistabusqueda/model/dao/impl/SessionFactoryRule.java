package com.creditoparati.robotlistabusqueda.model.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import com.creditoparati.robotlistabusqueda.model.Notario;

/**
 * Clase auxiliar para inyectar Session Factory a todas las pruebas
 * @author Daniel Vazquez
 *
 */
public class SessionFactoryRule implements MethodRule {
    private SessionFactory sessionFactory;
    private Transaction transaction;
    private Session session;
 
    @Override
    public Statement apply(final Statement statement, FrameworkMethod method, Object test) {
	 
	    return new Statement() {
	        @Override
	        public void evaluate() throws Throwable {
	            sessionFactory = createSessionFactory();
	            createSession();
	            beginTransaction();
	            try {
	                statement.evaluate();
	            } finally {
	                shutdown();
	            }
	        }
	    };
	}
 
    private void shutdown() {
        try {
            try {
                try {
                	transaction.rollback();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                session.close();
            } catch (Exception ex) {
            	ex.printStackTrace();
            }
            sessionFactory.close();
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
    }
 
    private SessionFactory createSessionFactory() {
    	 AnnotationConfiguration configuration = new AnnotationConfiguration();
  
 		configuration.addAnnotatedClass(Notario.class);	
		
 		configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
 		configuration.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
 		configuration.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/cpt");
 		configuration.setProperty("hibernate.connection.username", "cpt");
 		configuration.setProperty("hibernate.connection.password", "cpt");
 		configuration.setProperty("hibernate.hbm2ddl.auto", "create");
 		
 		SessionFactory sessionFactory = configuration.buildSessionFactory();
 		
 		session = sessionFactory.openSession();
        return sessionFactory;
    }
 
    public Session createSession() {
    	session = sessionFactory.openSession();
    	return session;
    }
 
    public void commit() {
    	transaction.commit();
    }
 
    public void beginTransaction() {
    	transaction = session.beginTransaction();
    }
 
    public Session getSession() {
    	return session;
    }
}

