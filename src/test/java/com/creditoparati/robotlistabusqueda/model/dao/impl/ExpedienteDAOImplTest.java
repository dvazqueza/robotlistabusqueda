/**
 * 
 */
package com.creditoparati.robotlistabusqueda.model.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;  
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;  
import org.springframework.test.context.transaction.TransactionConfiguration;  

import com.creditoparati.robotlistabusqueda.model.Expediente;
import com.creditoparati.robotlistabusqueda.model.dao.ExpedienteDAO;

/**
 * @author Daniel Vazquez
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations="classpath:service-context.xml")  
@TransactionConfiguration(defaultRollback=true,transactionManager="transactionManager")  
public class ExpedienteDAOImplTest {

//	@Rule
//	public final SessionFactoryRule sf = new SessionFactoryRule();

    /**
     * Logging.
     */
    private static final Logger log = Logger.getLogger(ExpedienteDAOImplTest.class);
    
	@Autowired
	private ExpedienteDAO expedienteDAO;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void before() throws Exception {
		
	}
	

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void after() throws Exception {

		
	}

	/**
	 * Test method for {@link com.creditoparati.robotlistabusqueda.model.dao.impl.ExpedienteDAOImpl#getById(String)}.
	 */
	@Test
	public void testGetById() throws Exception {
		
		log.debug(">>------ Iniciando testGetById");
		
		String[] nombreCliente = {"MARIA DE LAS MERCEDES PEREA HERNANDEZ",
				"NICOLAS DZIB UH",
				"ALVARO ANTONIO SANCHEZ Y PINTO"};
		String[] numeroSolicitud = {"201301339045", "201301341348", "201301340372"};
		
		for(int i =0; i < numeroSolicitud.length; i++){
			Expediente expediente = expedienteDAO.getById(numeroSolicitud[i]);
			
			assertNotNull(expediente);
			assertEquals(nombreCliente[i], expediente.getNombre());
		}
	}

	/**
	 * Test method for {@link com.creditoparati.robotlistabusqueda.model.dao.impl.ExpedienteDAOImpl#getTotalExpedientes()}.
	 */
	@Test
	public void testGetTotalExpedientes() throws Exception {
		log.debug(">>------ Iniciando testGetTotalExpedientes");
		
		long numeroExpedientes = expedienteDAO.getTotalExpedientes();
		assertEquals(34, numeroExpedientes);
		
	}

	/**
	 * Test method for {@link com.creditoparati.robotlistabusqueda.model.dao.impl.ExpedienteDAOImpl#loadAll()}.
	 */
	@Test
	public void testLoadAll() throws Exception {
		log.debug(">>------ Iniciando testLoadAll");
		
		List<Expediente> expedientes = expedienteDAO.loadAll();
		assertNotNull(expedientes);
		assertEquals(34, expedientes.size());
		
	}
	
	/**
	 * Test method for {@link com.creditoparati.robotlistabusqueda.model.dao.impl.ExpedienteDAOImpl#delete()}.
	 */
	@Test
	public void testDelete() throws Exception {
		log.debug(">>------ Iniciando testDelete");
		String idExpediente = expedienteDAO.getSiguienteIdExpediente().toString();
		log.debug("Siguiente Expediente =[" + idExpediente + "]");
		String nombre = "TEXTO PRUEBA";
		String numeroCliente = "12345";
		
		Expediente transientInstance = new Expediente();
		// transientInstance.setNumeroSolicitud(idExpediente);
		transientInstance.setNombre(nombre);
		transientInstance.setNumeroCliente(numeroCliente);
		
		expedienteDAO.save(transientInstance);
		
		Expediente desdeBase = expedienteDAO.getById(idExpediente);
		
		assertNotNull(desdeBase);
		assertEquals(nombre, desdeBase.getNombre());
		
		expedienteDAO.delete(idExpediente);
		
		Expediente despuesBorrar = expedienteDAO.getById(idExpediente);
		
		assertNull(despuesBorrar);
	
	}
}
