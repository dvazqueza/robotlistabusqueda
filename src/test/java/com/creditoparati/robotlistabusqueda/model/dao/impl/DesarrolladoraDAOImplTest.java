/**
 * 
 */
package com.creditoparati.robotlistabusqueda.model.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;  
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;  
import org.springframework.test.context.transaction.TransactionConfiguration;  

import com.creditoparati.robotlistabusqueda.model.Desarrolladora;
import com.creditoparati.robotlistabusqueda.model.dao.DesarrolladoraDAO;

/**
 * @author Daniel Vazquez
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations="classpath:service-context.xml")  
@TransactionConfiguration(defaultRollback=true,transactionManager="transactionManager")  
public class DesarrolladoraDAOImplTest {

//	@Rule
//	public final SessionFactoryRule sf = new SessionFactoryRule();

    /**
     * Logging.
     */
    private static final Logger log = Logger.getLogger(DesarrolladoraDAOImplTest.class);
    
	@Autowired
	private DesarrolladoraDAO desarrolladoraDAO;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void before() throws Exception {
		
	}
	

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void after() throws Exception {

		
	}

	/**
	 * Test method for {@link com.creditoparati.robotlistabusqueda.model.dao.impl.DesarrolladoraDAOImpl#getTotalDesarrolladoras()}.
	 */
	@Test
	public void testGetTotalDesarrolladoras() throws Exception {
		log.debug(">>------ Iniciando testGetTotalDesarrolladoras");
		
		long numeroDesarrolladoras = desarrolladoraDAO.getTotalDesarrolladoras();
		assertEquals(112, numeroDesarrolladoras);
		
	}

	/**
	 * Test method for {@link com.creditoparati.robotlistabusqueda.model.dao.impl.DesarrolladoraDAOImpl#loadAll()}.
	 */
	@Test
	public void testLoadAll() throws Exception {
		log.debug(">>------ Iniciando testLoadAll");
		
		List<Desarrolladora> desarrolladoras = desarrolladoraDAO.loadAll();
		assertNotNull(desarrolladoras);
		assertEquals(112, desarrolladoras.size());
		
	}
	
	/**
	 * Test method for {@link com.creditoparati.robotlistabusqueda.model.dao.impl.DesarrolladoraDAOImpl#getByNombre(String)}.
	 */
	@Test
	public void testGetByNombre() throws Exception{
		
		log.debug(">>------ Iniciando testGetByNombre");
		
		String[] nombre = {"CASAS PROCSA S.A. DE C.V ",
				"DESARROLLOS INMOBILIARIOS SADASI S.A. DE C.V. (AMERICAS)",
				"SADASI HEROES MERIDA"};
		long[] numeroDesarrolladora = {2, 13, 17};
		
		for(int i =0; i < nombre.length; i++){
			log.debug("Desarrolladora = [" + nombre[i] + "]");
			Desarrolladora desarrolladora = desarrolladoraDAO.getByNombre(nombre[i]);
			
			assertNotNull(desarrolladora);
			assertEquals(numeroDesarrolladora[i], desarrolladora.getId());
		}
	}
	
	/**
	 * Test method for {@link com.creditoparati.robotlistabusqueda.model.dao.impl.NoptarioDAOImpl#getByNombreNotario(String)}.
	 */
	@Test
	public void testGetByNombreCorto() throws Exception{
		
		log.debug(">>------ Iniciando testGetByNombreCorto");
		
		String[] nombreCorto = {"PROCSA",
				"SADASI AMERICAS",
				"SAD HEROES"};
		long[] numeroDesarrolladora = {2, 13, 17};
		
		for(int i =0; i < nombreCorto.length; i++){
			Desarrolladora desarrolladora = desarrolladoraDAO.getByNombreCorto(nombreCorto[i]);
			
			assertNotNull(desarrolladora);
			assertEquals(numeroDesarrolladora[i], desarrolladora.getId());
		}
	}

}
