/**
 * 
 */
package com.creditoparati.robotlistabusqueda.model.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;  
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;  
import org.springframework.test.context.transaction.TransactionConfiguration;  

import com.creditoparati.robotlistabusqueda.model.Notario;
import com.creditoparati.robotlistabusqueda.model.dao.NotarioDAO;

/**
 * @author Daniel Vazquez
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations="classpath:service-context.xml")  
@TransactionConfiguration(defaultRollback=true,transactionManager="transactionManager")  
public class NotarioDAOImplTest {

//	@Rule
//	public final SessionFactoryRule sf = new SessionFactoryRule();

    /**
     * Logging.
     */
    private static final Logger log = Logger.getLogger(NotarioDAOImplTest.class);
    
	@Autowired
	private NotarioDAO notarioDAO;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void before() throws Exception {
		
	}
	

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void after() throws Exception {

		
	}

	/**
	 * Test method for {@link com.creditoparati.robotlistabusqueda.model.dao.impl.NoptarioDAOImpl#getByNombreNotario(String)}.
	 */
	@Test
	public void testGetByNombreNotario() {
		
		log.debug(">>------ Iniciando testGetByNombreNotario");
		
		String[] nombreNotario = {"OCTAVIO ANDRES MARTINEZ TELLEZ",
				"NICOLAS MALOFF MALUF",
				"ANGEL ENRIQUE NU�EZ AGUILAR"};
		long[] numeroNotaria = {2, 13, 17};
		
		for(int i =0; i < nombreNotario.length; i++){
			Notario notario = notarioDAO.getByNombreNotario(nombreNotario[i]);
			
			assertNotNull(notario);
			assertEquals(numeroNotaria[i], notario.getNumNotaria());
		}
	}

	/**
	 * Test method for {@link com.creditoparati.robotlistabusqueda.model.dao.impl.NoptarioDAOImpl#getByNombreNotario(String)}.
	 */
	@Test
	public void testGetByNumNotario() {
		log.debug(">>------ Iniciando testGetByNumNotario");
		
		long[] numeroNotaria = {2, 13, 17};
		long[] numeroNotariaElem = {3, 3, 1};
		
		for(int i =0; i < numeroNotaria.length; i++){
			log.debug("Notaria = [" + numeroNotaria[i] + "]");
			List<Notario> listaNotario = notarioDAO.getByNumNotaria(numeroNotaria[i]);
			
			assertNotNull(listaNotario);
			assertEquals(numeroNotariaElem[i], listaNotario.size());
			for(Notario elemento:listaNotario)
			log.debug("Elementos = [" + elemento.getNombre() + "]");
		}
		
	}
}
