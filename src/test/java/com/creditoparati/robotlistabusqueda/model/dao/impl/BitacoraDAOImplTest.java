/**
 * 
 */
package com.creditoparati.robotlistabusqueda.model.dao.impl;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;  
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;  
import org.springframework.test.context.transaction.TransactionConfiguration;  

import com.creditoparati.robotlistabusqueda.model.Bitacora;
import com.creditoparati.robotlistabusqueda.model.dao.BitacoraDAO;

/**
 * @author Daniel Vazquez
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations="classpath:service-context.xml")  
@TransactionConfiguration(defaultRollback=true,transactionManager="transactionManager")  
public class BitacoraDAOImplTest {

//	@Rule
//	public final SessionFactoryRule sf = new SessionFactoryRule();

    /**
     * Logging.
     */
    private static final Logger log = Logger.getLogger(BitacoraDAOImplTest.class);
    
	@Autowired
	private BitacoraDAO bitacoraDAO;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void before() throws Exception {
		
	}
	

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void after() throws Exception {

		
	}

	/**
	 * Test method for {@link com.creditoparati.robotlistabusqueda.model.dao.impl.ExpedienteDAOImpl#getById(String)}.
	 */
	@Test
	public void testGetById() throws Exception {
		
		log.debug(">>------ Iniciando testGetById");
		
		String[] nombreCliente = {"ERASMO RODRIGUEZ  RIOS",
				"CLAUDIA LUCIA NAVA SERRANO"};
		long[] numeroSolicitud = {3477, 3708};
		
		for(int i =0; i < numeroSolicitud.length; i++){
			Bitacora expediente = bitacoraDAO.getById(numeroSolicitud[i]);
			
			assertNotNull(expediente);
			assertEquals(nombreCliente[i], expediente.getNombre());
		}
	}

	/**
	 * Test method for {@link com.creditoparati.robotlistabusqueda.model.dao.impl.ExpedienteDAOImpl#getTotalExpedientes()}.
	 */
	@Test
	public void testGetTotalExpedientesProcesadosPorTiempo() throws Exception {
		log.debug(">>------ Iniciando testGetTotalExpedientesProcesadosPorTiempo");
		
		String tiempo = "2013-04-29";
		long numeroExpedientes = bitacoraDAO.getTotalExpedientesProcesadosPorTiempo(tiempo);
		assertEquals(34, numeroExpedientes);
		
	}

	/**
	 * Test method for {@link com.creditoparati.robotlistabusqueda.model.dao.impl.ExpedienteDAOImpl#delete()}.
	 */
	@Test
	public void testSave() throws Exception {
		log.debug(">>------ Iniciando testDelete");
		long idBitacora = bitacoraDAO.getSiguienteIdBitacora();
		log.debug("Siguiente Bitacora =[" + idBitacora + "]");
		String nombre = "TEXTO PRUEBA";
		String numeroExpediente = "12345";
		
		Bitacora transientInstance = new Bitacora();
		// transientInstance.setNumeroSolicitud(idExpediente);
		transientInstance.setNombre(nombre);
		transientInstance.setExpediente(numeroExpediente);
		transientInstance.setFechaFirma(new Date());
		
		bitacoraDAO.save(transientInstance);
		
		Bitacora desdeBase = bitacoraDAO.getById(idBitacora);
		
		assertNotNull(desdeBase);
		assertEquals(nombre, desdeBase.getNombre());
		
		bitacoraDAO.delete(idBitacora);
		
		Bitacora despuesBorrar = bitacoraDAO.getById(idBitacora);
		
		assertNull(despuesBorrar);
	
	}
}
